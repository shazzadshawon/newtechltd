@extends('layouts.backend')
@section('content')

<div class="row ">
    <div class="box col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Offer</h2>
            <div class="box-icon">

                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        
        <div class="">
            <div class="">
             	{!! Form::open(['route' => 'offer.store','files'=>true, 'class'=>'form-horizontal']) !!}
                <fieldset>
                <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Select Main Category</label>
                        <div class="col-md-10">
                            <select name="pazzle" class="form-control">
                               
                                  @php
                        $mainCategory = DB::table('categories')
                        ->where('publication_status',1)
                        ->get();
                                @endphp
                                @foreach ($mainCategory as $mainCategoryInfo)
                            
                              
                                   <option value="{{ $mainCategoryInfo->category_id }}">{{ $mainCategoryInfo->category_name }}</option>

                                  @endforeach
                                  
                            </select>
                        </div>
                    </div>

                <!--<div class="form-group">-->
                <!--        <label class="control-label col-md-2" for="typeahead">Offer URL</label>-->
                <!--        <div class="col-md-10">-->
                <!--            <input type="text"  name="heading" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">-->
                           
                <!--        </div>-->
                <!--    </div>-->
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Select Images</label>
                        <div class="col-md-10">
                        {!! Form::file('pazzle_image', array('multiple'=>false, 'class'=>'form-control')) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-2" for="date01"></label>
                       <div class="col-md-10">
                            <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                       </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection