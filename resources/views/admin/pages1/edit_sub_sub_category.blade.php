@extends('layouts.backend')
@section('content')
<div class="row sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
              @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif
        
        <div class="box-content">
            <div class="box-content">
             	{!! Form::model($sub_sub_category, ['route' => ['sub-sub-category.update',$sub_sub_category->id], 'method' => 'PUT', 'name'=>'edit_sub_category']) !!}
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="date01">Category</label>
                        <div class="controls">                            
                            <select name="sub_category_id">
                                @php
                                    $subSubCategory = DB::table('sub_categories')->get();
                                @endphp
                                <option >====Select Category===</option>
                                @foreach ($subSubCategory as $subSubCategoryInfo)
                                <option value="{{$subSubCategoryInfo->sub_category_id}}">{{$subSubCategoryInfo->sub_category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sub Category Name</label>
                        <div class="controls">
                            <input type="text"  name="sub_sub_category_name" value="{{$sub_sub_category->sub_sub_category_name}}" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">সাব ক্যাটাগরি নাম</label>
                        <div class="controls">
                            <input type="text"  name="sub_sub_category_name_bn" value="{{$sub_sub_category->sub_sub_category_name_bn}}" class="span6 typeahead" id="typeahead"  data-provide="typeahead" data-items="4">
                           
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="date01">Publication Status</label>
                        <div class="controls">
                            <select name="publication_status">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<script>   
    document.forms['edit_sub_category'].elements['sub_category_id'].value = '{{$sub_sub_category->sub_category_id}}';  
    document.forms['edit_sub_category'].elements['publication_status'].value = '{{$sub_sub_category->publication_status}}';  
</script>
@endsection

