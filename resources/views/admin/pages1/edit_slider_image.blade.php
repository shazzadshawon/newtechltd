@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-9">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Slider Image</h2>
            <div class="box-icon">
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
             
        <div class="box-content">
            <div class="box-content">
             	{!! Form::model($slider_image, ['route' => ['slider-image.update',$slider_image->slider_image_id], 'method' => 'PUT', 'files'=>true,'name'=>'edit_slider_image','class'=>'form-horizontal']) !!}
                <fieldset>


                     <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Title</label>
                        <div class="col-md-10">
                            <input type="text"  name="title" class="form-control" id=""  value="{{ $slider_image->title }}" data-items="" >
                           
                          
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Subtitle</label>
                        <div class="col-md-10">
                            <input type="text"  name="subtitle" class="form-control" value="{{ $slider_image->subtitle }}" >
                           
                          
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Select Images</label>
                        <div class="col-md-10">
<!--                            {!! Form::file('slider_image') !!}-->
                            <input type="file"  name="slider_image" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4" >
                            <img src="{{ asset('slider_image/'.$slider_image->slider_image) }}" style="height: 100px; width: 150px;">
                          
                        </div>
                    </div>
                   
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                            @if($slider_image->publication_status == 1)
                                <option value="1" selected>Published</option>
                                <option value="0">Unpublished</option>
                            @else
                                <option value="0" selected>Unpublished</option>
                                <option value="1" >Published</option>
                            @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                     <label class="control-label col-md-2" for="typeahead">Subtitle</label>
                     <div class="col-md-10">
                         
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn">Cancel</button>
                     </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<script>
    document.forms['edit_slider_image'].elements['publication_status'].value = {{$slider_image->publication_status}};    
</script>
@endsection

