@extends('layouts.frontend')

@section('content')
<!-- TOP BANNER -->
<!-- <div id="top-banner" class="top-banner">
    <div class="bg-overlay"></div>
    <div class="container">
        <h1>Special Offer!</h1>
        <h2>Additional 40% OFF For Men & Women Clothings</h2>
        <span>This offer is for online only 7PM to middnight ends in 30th July 2015</span>
        <span class="btn-close"></span>
    </div>
</div> -->

<!-- Home slideder-->
    @include('frontend/home_slider')
<!-- END Home slideder-->
<!-- Free Shipping servives -->
<div class="container">
    <div class="service ">
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="assets/data/s1.png" />
            </div>
            <div class="info">
                <a href="#"><h3>Free Shipping</h3></a>
                <span>Dhaka- Free, Outside Dhaka: 65 TK</span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="assets/data/s2.png" />
            </div>
            <div class="info">
                <a href="#"><h3>Delivery Time</h3></a>
                <span>Delivery Within 1-5 business days</span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="assets/data/s3.png" />
            </div>
            
            <div class="info" >
                <a href="#"><h3>24/7 support</h3></a>
                <span>Hotline: +8801611392222 What’s App: 01790504256 </span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="assets/data/s4.png" />
            </div>
            <div class="info">
                <a href="#"><h3>SAFE SHOPPING</h3></a>
                <span>Safe Shopping Guarantee</span>
            </div>
        </div>
    </div>
</div>
<!-- end Free Shipping servives -->

<!--Category Contents-->
<div class="content-page">
    <div class="container">
        

        <!-- featured category fashion -->
       
        <!-- end featured category fashion -->

        <!-- featured category fashion -->
      @foreach($main_categories as $main)
      @php
          $m = $main; 
      @endphp
        <div class="category-featured">     <!-- men -->
            <nav class="navbar nav-menu nav-menu-red show-brand">
              <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-brand"><a href="{{ url('Main-Category-products/'.$main->category_id) }}"><img alt="fashion" src="assets/data/fashion.png" />{{ $main->category_name }}</a></div>
                <span class="toggle-menu"></span>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse">           
                  <ul class="nav navbar-nav">
                  @php
                    $sub_categories = DB::table('sub_categories')->where('category_id', $main->category_id)->take(4)->get();
                    $cat = $sub_categories;
                    $i=1;
                  @endphp

                    @foreach ($sub_categories as $sub)
                        <li class="@if ($i==1) active @endif"><a data-toggle="tab" href="#{{ $sub->sub_category_id }} ">{{ $sub->sub_category_name }}</a></li>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                   
                    
                   
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
             {{--  <div id="elevator-2" class="floor-elevator">
                    <a href="#elevator-1" class="btn-elevator up fa fa-angle-up"></a>
                    <a href="#elevator-3" class="btn-elevator down fa fa-angle-down"></a>
              </div> --}}
            </nav>
            <div class="product-featured clearfix">
                <div class="banner-featured">
                @php
                  
                    $offer = DB::table('pazzles')->where('pazzle',$main->category_id)->where('publication_status',1)->orderBy('id', 'desc')->first();
                       
                        $pzl =  (array) $offer;
                @endphp
                    <div class="featured-text"><span></span></div>
                   {{-- {{ $pzl['heading'] }} --}}
                    <div class="banner-img">
                       {{--  <a href="#"> --}}
                        @php
                           // echo "<pre>";
                           // print_r($pzl['pazzle_image']);
                        @endphp
                        @if (!empty($pzl))
                             <a href="#" target="">
                                 <img alt="Featurered 1" style="height: 320px;" src="{{ asset('/'.$pzl['pazzle_image']) }}" />
                             </a>
                        @endif
                        
                    </div>
                </div>
                <div class="product-featured-content">
                    <div class="product-featured-list">
                        <div class="tab-container">
                            <!-- tab product -->
                            @php
                                $a=1;
                            @endphp
                            @foreach ($sub_categories as $sub)
                            <div class="tab-panel @if ($a==1) active @endif" id="{{ $sub->sub_category_id }}">   <!-- SHAREE -->
                                <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "0" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
                                @php
                        $pros = DB::table('products')->where('sub_category_id',$sub->sub_category_id)->get();
                                    $products = array();
                                    foreach ($pros as $p)
                                    {
                                        $products[] = (array)$p;
                                    }
                                    // print_r($products);
                                    // exit();
                                    
                                @endphp

                                @foreach($products as $product)
                                    <li>
                                    @php
                                        $image = DB::table('product_images')->where('product_id',$product['id'])->first();
                                    @endphp
                                  
                                        <div class="left-block">
                                            <a href="{{ url('product-details/'.$product['id']) }}">
                                            <img style="height: 250px" class="img-responsive" alt="pants" src="{{  asset('/product_image/'.$image->product_image) }}" /></a>
                                            <div class="quick-view">
                                @php
                                 
                                        $wishlist = DB::table('wishlists')
                                                ->where('product_id', $product['id'])
                                                ->where('customer_id', Session::get('customer_id'))->first();
                                @endphp
                                          
                                   {{-- wish --}}
                     
                                        @if (Session::has('customer_id'))
                                      
                                        @if($wishlist==NULL)
                                        {!! Form::open(['route' => 'wishlist.store','files'=>true]) !!}
                                         <input type="hidden" name="customer_id" value="{{Session::get('customer_id')}}">
                                        <input type="hidden" name="product_id" value="{{$product['id']}}">
                                       <div class="">
                                      
                                        <button type="submit" title="Add to my wishlist" class="fa fa-heart-o" value="W" style="color: green;"></button>
                                     
                                       </div>
                                         {!! Form::close() !!}
                                          @else
                                          <div class="">
                                        <a title="Add to my wishlist" class="fa fa-heart-o" style="color: blue;"></a>
                                     
                                             </div>
                                          @endif
                                        @else 
                                        <div class="">
                                        <a title="Add to my wishlist" class="fa fa-heart-o" href="{{URL::to('/User-Register')}}"></a>
                                     
                                        </div>
                                   
                                        @endif
                               

                               {{-- Wish end --}}
                                                   {{--  <a title="Add to my wishlist" class="fa fa-heart-o" href="#"></a> --}}
                                                  {{--   <a title="Add to compare" class="compare" href="#"></a>
                                                    <a title="Quick view" class="search" href="#"></a> --}}
                                            </div>
                                            <div class="add-to-cart">
                                                 {!! Form::open(['route' => 'Add-To-Cart.store','files'=>true, 'class'=>'cart']) !!}   

                                <input type="hidden" name="product_id" value="{{$product['id']}}">
                                <input type="hidden" name="product_name" value="{{$product['product_name']}}">
                               {{--  <input type="hidden" name="product_name_bn" value="{{$product->product_name_bn}}"> --}}
                                <input type="hidden" name="product_code" value="{{$product['product_code']}}">
                                <input type="hidden" name="product_price" value="@if($product['discount'] > 0){{ $product['product_price']-($product['product_price']*$product['discount'])/100}}@else{{$product['product_price']}}@endif">
                                <input type="hidden" name="product_quantity" value="1">
                                <input type="hidden" name="publication_status" value="{{$product['publication_status']}}">
                                                {{-- <a title="Add to Cart" href="#">Add to Cart</a> --}}
                                                <button class="" type="submit">
                                       
                                                 Add To Cart
                                            
                                                </button>
                                                 {!! Form::close() !!}
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="{{ url('product-details/'.$product['id']) }}">{{ $product['product_name'] }}</a></h5>
                                            <div class="content_price">
                                                <span class="price product-price">&#2547;@php
                                                    print_r($product['product_price']);
                                                @endphp</span>
                                                {{-- <span class="price old-price">&#2547;@php
                                                    print_r($product['id']);
                                                @endphp</span> --}}
                                            </div>
                                            <div class="product-star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                                <i class="fa fa-star-o"></i>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                            @php
                                $a++;
                            @endphp
                            @endforeach




                        </div>   
                    </div>
                </div>
            </div>
        </div>
      @endforeach
        <!-- end featured category fashion -->

        
{{-- @include('frontend/ads') --}}
        <!-- end banner bottom -->
    </div>
</div>




@endsection