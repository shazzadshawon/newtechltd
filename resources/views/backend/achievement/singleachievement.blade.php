@extends('layouts.backend')

@section('content') 
    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            
                            <div class="panel panel-primary">
                            <div class="panel-heading">
                             <div class="post-">
                                           <h4> {{ $achievement->title }}</h4>
                                           
                                        </div></div>
                                <div class="panel-body posts">
                                            
                                    <div class="post-item">
                                       
                                         <div align="right" >
                                                
                                            </div>
                                        <div class="post-text">                                            
                                        <img style="width: 100%; height: 300px" class="img img-responsive" src="{{asset('public/uploads/achievement/'.$achievement->image)}}">
                                            <p>
                                                <?php print_r($achievement->description); ?>
                                            </p>
                                        </div>
                                       
                                    </div>                                            
                                            
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
@endsection