
@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Category</h3>
                                    <div class="pull-right">
                                        <a class="btn btn-primary" href="{{ url('add-category') }}">Add New Category</a>
                                    </div>
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
  <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Sl no.</th>
                        <th>Category Name</th>              
                         <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php 
                     $i=1;
                      ?>
                    @foreach ($categories as $category_info)
                   
                    <tr>
                        <td><?php echo $i;?></td>
                        <td class="center"><?php echo $category_info->category_name;?></td>                     
                        <td class="center">
                            <?php 
                            if($category_info->publication_status==1){
                            ?>
                            <span class="label label-success">Published</span>
                            <?php }else{ ?>
                             <span class="label label-danger">Unpublished</span>
                            <?php }?>
                        </td>
                                                <td>
                                                     <?php 
                            if($category_info->publication_status==1){
                            ?>
                            <a class="btn btn-primary" href="{{URL::to('/unpublished-category/'.$category_info->category_id)}}">
                                <i class="halflings-icon white thumbs_down"></i> Unpublish 
                            </a>
                             <?php }else{ ?>
                            <a class="btn btn-primary" href="{{URL::to('/published-category/'.$category_info->category_id)}}">
                                <i class="halflings-icon thumbs_up"></i>  Publish
                            </a>
                             <?php }?>
                            <a class="btn btn-info" href="{{URL::to('/category/'.$category_info->category_id.'/edit')}}">
                                <i class="halflings-icon white edit"></i>  Edit
                            </a>

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $category_info->category_id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $category_info->category_id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                         <a class="btn btn-danger" href="{{URL::to('/delete-category/'.$category_info->category_id)}}" >
                                <i class="fa fa-delete"></i> Delete
                            </a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                    <?php $i++; ?>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection


