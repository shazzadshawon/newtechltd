@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Media</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storegallery') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="gallery_image" value="" required autofocus>
                                        </div>
                                    </div>

                                    {{--  <div class="form-group">
                                        <label class="col-md-2 control-label">Choose Event</label>
                                        <div class="col-md-10">
                                           <select name="place" required class="form-control" >
                                               <option value="1">Wedding</option>
                                               <option value="2">Event</option>
                                               <option value="3">Music</option>
                                           </select>
                                        </div>
                                    </div> --}}
              
                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" value="Upload Image" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
        {{-- <div class="col-md-1"></div>
        <div class="col-md-5">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Gallery Video</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storegalleryvideo') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Video</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="video_name" value="" required autofocus>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Cover Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="cover_image" value="" required autofocus>
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Choose Category</label>
                                        <div class="col-md-10">
                                           <select name="place" required class="form-control" >
                                               <option value="1">Wedding</option>
                                               <option value="2">Event</option>
                                               <option value="3">Music</option>
                                           </select>
                                        </div>
                                    </div>
              
                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div> --}}
    </div>
@endsection