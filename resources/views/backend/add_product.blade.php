@extends('layouts.backend')
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        
        <div class="">
            <div class="">
             	{!! Form::open(['route' => 'product.store','files'=>true , 'class'=>'form-horizontal']) !!}
                <fieldset>
                     <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Main Category</label>
                        <div class="col-md-10">
                            <select name="category_id"  id="category" class="form-control">
                                <option value="0">====Main Select Category====</option>
                                @foreach($categories as $category_info)
                                <option value="{{$category_info->category_id}}">{{$category_info->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Category</label>
                        <div class="col-md-10">
                            <select name="sub_category_id" id="subcategory" class="form-control">
                                <option value="0">====Select Category====</option>
                                @foreach($sub_categories as $subcategories_info)
                                @php
                                    $category_name = DB::table('categories')->where('category_id',$subcategories_info->category_id)->first();
                                @endphp
                                @if (!empty($category_name))
                                     <option value="{{$subcategories_info->sub_category_id}}">{{$subcategories_info->sub_category_name.'('.$category_name->category_name.')'}}</option> 
                                @endif
                                @endforeach

                                
                                
                                
                            </select>
                        </div>
                    </div>
                

                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Name</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_name" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Code</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_code" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Price</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_price" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Quantity</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_quantity" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Discount</label>
                        <div class="col-md-10">
                            <input type="text"  name="discount" class="form-control" id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group hidden-phone">
                        <label class="control-label col-md-2" for="textarea2">Product Description</label>
                        <div class="col-md-10">
                            <textarea  name="description" class="cleditor form-control" id="editor1" rows="3"></textarea>
                        </div>
                    </div>
                   

                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Images</label>
                        <div class="col-md-10">
                            {!! Form::file('product_image', array('multiple'=>false, 'class'=>'form-control')) !!}
                           
                        </div>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label class="control-label col-md-2" for="date01">Select Product Type</label>--}}
                        {{--<div class="col-md-10">--}}
                            {{--<select name="offer_status" class="form-control">--}}
                                  {{--<option value="">Choose one</option>--}}
                                 {{----}}
                                {{--<option value="1">Featured</option>--}}
                                {{--<option value="2">New Arrival</option>--}}
                                {{--<option value="3">Top Sales</option>--}}
  {{----}}

                                {{----}}
                            {{--</select>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{----}}
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                                <option value="2">Stock Out</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                    <label class="control-label col-md-2" for="date01"></label>
                       <div class="col-md-10">
                            
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Cancel</button>
                       </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection