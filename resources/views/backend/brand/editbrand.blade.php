@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Brand</h4></div>
                 <div class="panel-body">
                    <div class="block">
                        <div class="row">
                            <div class="col-md-8">
                                   <form class="form-horizontal" method="POST" action="{{ url('updatebrand/'.$brand->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="title" value="{{ $brand->title }}" />
                                        </div>
                                    </div>
                                  {{--   <div class="form-group">
                                        <label class="col-md-2 control-label">Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="brand_type_id">
                                            <option value="{{ $brand->brand_type_id }}">{{ $brand->type_name }}</option>
                                               <option value="">Choose one</option>
                                                @foreach ($brandtypes as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->type_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Brand Logo</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="name" value="{{ old('name') }}"  autofocus onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}">
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea  name="editor1">@php
                                                    print_r($brand->description);
                                                @endphp</textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
                            </div>
                            <div class="col-md-4">
                                  <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/uploads/brand/'.$brand->image)}}" />
                            </div>
                        </div>          
                    </div>
                 </div>
             </div>
        </div>
    </div>
@endsection