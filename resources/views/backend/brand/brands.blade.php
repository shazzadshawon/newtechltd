@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Brands</h3>
                                    <div class="pull-right">
                                        <a class="btn btn-primary" href="{{ url('addbrand') }}"><span class="fa fa-plus"></span> New Brand</a>
                                    </div>
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style="width: 15%">Title</th>
                                                <th style="width: 10%">Logo</th>
                                               {{--  <th>Sub category</th>
                                                 --}}
                                                <th style="width: 40%">Description</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($brands as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->title }}</td>
                                                <td><img style="width: 100px; height: 60px" src="{{ asset('public/uploads/brand/'.$cat->image)  }}"></td>
                                              
                                                
                                                <td>@php
                                                    print_r($cat->description);
                                                @endphp</td>
                                                <td>
                                                    <a href="{{ url('editbrand/'.$cat->id) }}" class="btn btn-primary">Edit</a>
                                                    <a href="{{ url('singlebrand/'.$cat->id) }}" class="btn btn-warning">View</a> 
                                                     

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deletebrand/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection