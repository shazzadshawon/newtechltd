@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update About</h4></div>
                 <div class="panel-body">
                    <div class="block">
                        <div class="row">
                            <div class="col-md-8">
                                   <form class="form-horizontal" method="POST" action="{{ url('updateabout/'.$about->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">About Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="title" value="{{ $about->title }}" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Type</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="type_id">
                                                @if($about->type_id==0)
                                                    <option selected value="0">Office</option>
                                                    <option value="1">Showroom</option>
                                                @else
                                                    <option selected value="1">Showroom</option>
                                                    <option value="0">Office</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div> 

                                

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Cover Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="name" value=""  autofocus  onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}">
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea  name="editor1">@php
                                                    print_r($about->description);
                                                @endphp</textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
                            </div>
                            <div class="col-md-4">
                                  <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/uploads/about/'.$about->image)}}" />
                            </div>
                        </div>          
                    </div>
                 </div>
             </div>
        </div>
    </div>
@endsection