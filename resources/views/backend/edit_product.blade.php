@extends('layouts.backend')
@section('content')
<div class="row ">
    <div class="col-md-9">
        <div class="box-header">
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit Product</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
              @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h3> {{Session::get('message')}}</h3>
</div>
      
@endif
        
        <div class="">
            <div class="">
             	{!! Form::model($products, ['route' => ['product.update',$products->id], 'method' => 'PUT', 'name'=>'edit_product','class'=>'form-horizontal']) !!}
                <fieldset>
                     <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Category</label>
                        <div class="col-md-10">
                            <select name="category_id" id="type" class="form-control">
                                <option>====Select Category====</option>
                                @foreach($categories as $category_info)
                                <option value="{{$category_info->category_id}}">{{$category_info->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Sub Category</label>
                        <div class="col-md-10">
                            <select name="sub_category_id" id="subcategory" class="form-control">
                                <option>====Select Sub Category====</option>
                                @foreach($sub_categories as $subcategories_info)
                                @php
                                    $category_name = DB::table('categories')->where('category_id',$subcategories_info->category_id)->first();
                                @endphp
                                @if (!empty($category_name))
                                     <option value="{{$subcategories_info->sub_category_id}}">{{$subcategories_info->sub_category_name.'('.$category_name->category_name.')'}}</option> 
                                @endif
                               
                                @endforeach
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Name</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_name" value="{{$products->product_name}}" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Code</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_code" value="{{$products->product_code}}" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Price</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_price" value="{{$products->product_price}}" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Product Quantity</label>
                        <div class="col-md-10">
                            <input type="text"  name="product_quantity" value="{{$products->product_quantity}}" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="typeahead">Discount</label>
                        <div class="col-md-10">
                            <input type="text"  name="discount" value="{{$products->discount}}" class="form-control " id="typeahead"  data-provide="typeahead" data-items="4" >
                           
                        </div>
                    </div>
                    <div class="form-group hidden-phone">
                        <label class="control-label col-md-2" for="textarea2">Description</label>
                        <div class="col-md-10">
                            <textarea  name="description" class="form-control" id="editor1" rows="3">{{$products->description}}</textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Select Offer</label>
                        <div class="col-md-10">
                            <select name="offer_status" class="form-control">

                               
                                 
                                <option value="1">Featured</option>
                                <option value="2">New Arrival</option>
                                <option value="3">Top Sales</option>

                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2" for="date01">Publication Status</label>
                        <div class="col-md-10">
                            <select name="publication_status" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                                <option value="2">Stock Out</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                          <label class="control-label col-md-2" for="textarea2"></label>
                        <div class="col-md-10">
                             <button type="submit" class="btn btn-primary">Update</button>
                            <button type="reset" class="btn">Cancel</button>
                        </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
<script>   
    document.forms['edit_product'].elements['category_id'].value = '{{$products->category_id}}';  
    document.forms['edit_product'].elements['sub_category_id'].value = '{{$products->sub_category_id}}';  
     document.forms['edit_product'].elements['offer_status'].value = '{{$products->offer_status}}';  
    document.forms['edit_product'].elements['publication_status'].value = '{{$products->publication_status}}';  
</script>
<!--<script type="text/javascript">
$(document).ready(function(){

    $('#type').on("change",function () {
        var category_id = $(this).find('option:selected').val();
        $.ajax({
            url: "/select-sub-categories",
            type: "POST",
            data: "category_id="+category_id,
            success: function (response) {
                console.log(response);
                $("#type1").html(response);
            },
        });
    }); 

});

</script>-->
@endsection