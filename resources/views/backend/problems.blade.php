@extends('layouts.backend')
@section('content')
<div class="col-md-12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Problem List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
                     @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    {{Session::get('message')}}
</div>
      
@endif

        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Employee name</th>
                        <th>Project Name</th>
                        <th>Email</th>
                        <th>Problem Type</th>
                        <th>Problem Details</th>

                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                   @php
                       $i=1;
                   @endphp
                    <?php
                    foreach ($problems as $sub){
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $sub->employee_name;?></td>
                        <td><?php echo $sub->project_name;?></td>
                        <td><?php echo $sub->email;?></td>
                        <td><?php echo $sub->problem_type;?></td>
                        <td><?php echo $sub->problem_details;?></td>


                       
                        <td class="center">
                            
                           
                           
                            <a class="btn btn-danger" href="{{URL::to('/deleteproblem/'.$sub->id)}}" onclick="return checkDelete();">
                                <i class="halflings-icon white trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                    
                    <?php $i++;} ?>
                   
                </tbody>
            </table>            
        </div>
    </div>
@endsection