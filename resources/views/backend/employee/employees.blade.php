@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success box-shadow">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Users</h3>
                                    <div class="pull-right">
                                        <a class="btn btn-primary" href="{{ url('addemployee') }}"><span class="fa fa-plus"></span> Add Employee</a>
                                    </div>
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Image</th>
                                                <th style="width: 10%">Full Name</th>
                                                <th style="">Designation</th>
                                                <th style="">Role</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                
                                               {{--  <th>Sub Category</th>
                                                 --}}
                                                
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($employees as $element)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                
                                                <td><img style="width: 100px; height: 60px" src="{{ asset('public/uploads/employee/'.$element->image)  }}"></td>
                                                <td>{{ $element->name }}</td>
                                                <td>@php
                                                    print_r($element->designation);
                                                @endphp</td>
                                                <td>
                                                @if ($element->role ==1)
                                                    Admin
                                                @elseif($element->role ==2)
                                                    Employee
                                                @else
                                                    Unknown
                                                @endif
                                                </td>
                                                <td>{{ $element->email }}</td>
                                              
                                                
                                                <td>@php
                                                    print_r($element->address);
                                                @endphp</td>
                                                
                                                <td>
                                                    <a href="{{ url('editemployee/'.$element->id) }}" class="btn btn-primary">Edit</a>
                                                   {{--  <a href="{{ url('singleemployee/'.$element->id) }}" class="btn btn-warning">View</a> 
                                                      --}}

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $element->id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $element->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deleteemployee/'.$element->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div> 
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection