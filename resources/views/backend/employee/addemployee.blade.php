@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-success box-shadow">
                 <div class="panel-heading"><h4>Add New Employee</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                        <div class="row">
                            <div class="col-md-8">
                                
                                 <form class="form-horizontal" method="POST" action="{{ url('storeemployee') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Full Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="name" required="" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Role</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="role" required="">
                                            <option value="">Choose one</option>
                                               <option value="1">Admin</option>
                                               <option value="2">Employee</option>
                                            </select>
                                        </div>
                                    </div>

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" required type="file" class="form-control" name="image" value="{{ old('image') }}"  autofocus onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])" value="{{ asset('') }}">
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Designation </label>
                                         <div class="col-md-10">
                                            <input type="text" class="form-control" name="designation"/>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-2 control-label">Email ID </label>
                                         <div class="col-md-10">
                                            <input type="email" class="form-control" name="email" required="" />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Address </label>
                                         <div class="col-md-10">
                                            <textarea class="form-control" name="address"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Password </label>
                                         <div class="col-md-10">
                                            <input type="password" class="form-control" name="password" required="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Confirm Password </label>
                                         <div class="col-md-10">
                                            <input type="password" class="form-control" name="confirmpassword" required="" />
                                        </div>
                                    </div>
                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>


                            </div>
                            <div class="col-md-4">
                                <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/demo.png')}}" />
                            </div>
                        </div>
                                  
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection