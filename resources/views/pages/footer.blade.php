<footer id="footer2">
     <div class="footer-top">
         <div class="container">
             <div class="row">
                 <div class="col-sm-3">
                     <div class="footer-logo">
                         <a href="#"><img src="../images/logo.jpg" alt="Logo"></a>
                     </div>
                 </div>
                 <div class="col-sm-6">
                     <div class="footer-menu">       
                         <ul>
                             <li><a href="{{ URL::to('/About-Us') }}">
                            @if (Session::has('EN'))
                                           About Us
                                         @else
                                           এবাউট আস
                                         @endif
                             </a></li>
                             </a></li>
                             <li><a href="{{ URL::to('/Tream-Condition') }}">
                                 @if (Session::has('EN'))
                                           Terms & Conditions
                                         @else
                                           টার্মস & কন্ডিশন
                                         @endif
                             </a></li>
                           
                           <li><a href="{{ URL::to('/Tream-Condition') }}">
                                        @if (Session::has('EN'))
                                           Privacy Policy
                                         @else
                                           প্রাইভেসি পলিসি
                                         @endif
                             </a></li>
                            
                             <li><a href="{{ URL::to('/Tream-Condition') }}">
                                        @if (Session::has('EN'))
                                           Contact Us
                                         @else
                                           কন্টাক্ট আস
                                         @endif
                             </a></li>
                         </ul>
                     </div>
                 </div>
                 <div class="col-sm-3">
                     <div class="footer-social">
                         <ul>
                             <li><a class="facebook" href="https://www.facebook.com/Kenakatazonecom-627558324114668/"><i class="fa fa-facebook"></i></a></li>
                             <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                             <li><a class="vk" href="#"><i class="fa fa-vk"></i></a></li>
                             <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                             <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                         </ul>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- footer paralax-->
     <div class="footer-paralax">
         <div class="footer-row footer-center">
             <div class="container">
                 <h3>
                               @if (Session::has('EN'))
                              Sign up below for early updates
                                @else
                             আধুনিক আপডেটগুলির জন্য নীচে সাইন আপ করুন
                                @endif
                 
                 </h3>
                 <p>You a Client , large or small, and want to participate in this adventure, please send us an email to support@Kenakatazone.com</p>                 
                    {!! Form::open(['route' => 'subscribe.store', 'class'=>'form-inline form-subscribe']) !!}
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="Enter Your E-mail Address" name="email">
                        <button type="submit" class="btn btn-default"><i class="fa fa-paper-plane-o"></i></button>
                      </div>
                      {!! Form::close() !!}
                    
                
             </div>
         </div>
         <div class="footer-row">
             <div class="container">
                 <div class="row">
                     <div class="col-sm-3">
                         <div class="widget-container">
                             <h3 class="widget-title">
                                 @if (Session::has('EN'))
                                          Infomation
                                         @else
                                           ইনফমেশন
                                         @endif
                                 
                                 </h3>
                             <div class="widget-body">
                                 <ul>
                                     <li><a class="location" href="#"></a></li>
                                     <li><a class="phone" href="#">
                  @if (Session::has('EN'))
                        01707744455, 01707744466             
                  @else
                       ০১৭০৭৭৪৪৪৫৫, ০১৭০৭৭৪৪৪৬৬
                  @endif
                                     </a></li>
                                     <li><a class="email" href="#">nfo@Kenakatazone.com</a></li>
                                     <li><a class="mobile" href="#"></a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-3">
                         <div class="widget-container">
                             <h3 class="widget-title">
                                 @if (Session::has('EN'))
                                          COMPANY
                                         @else
                                           কোম্পানি
                                         @endif
                                 
                                 </h3>
                             <div class="widget-body">
                                 <ul>
                                     <li><a href="{{ URL::to('/About-Us') }}">@if (Session::has('EN'))
                                           About Us
                                         @else
                                           এবাউট আস
                                         @endif</a></li>
                                     <li><a href="#">Testimonials</a></li>
                                     <li><a href="#">Affiliate Program</a></li>
                                     <li><a href="{{ URL::to('/Tream-Condition') }}">
                                         @if (Session::has('EN'))
                                           Terms & Conditions
                                         @else
                                           টার্মস & কন্ডিশন
                                         @endif
                                     </a></li>
                                     <li><a href="#">Contact Us</a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-3">
                         <div class="widget-container">
                             <h3 class="widget-title">
                                 @if (Session::has('EN'))
                                          my account
                                         @else
                                           মাই একাউন্ট
                                         @endif
                                 
                                 </h3>
                             <div class="widget-body">
                                 <ul>
                                     <li><a href="#">My Orders</a></li>
                                     <li><a href="#">My Credit Slips</a></li>
                                     <li><a href="#">My Addresses</a></li>
                                     <li><a href="#">My Personal Info</a></li>
                                     <li><a href="#">Specials</a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-3">
                         <div class="widget-container">
                             <h3 class="widget-title">
                                @if (Session::has('EN'))
                                          SUPPORT
                                         @else
                                           সাপোর্ট
                                         @endif
                                 
                                 </h3>
                             <div class="widget-body">
                                 <ul>
                                     <li><a href="#">Payments & My Vouchers</a></li>
                                     <li><a href="#">Saved Cards</a></li>
                                     <li><a href="#">Shipping Free</a></li>
                                     <li><a href="#">Cancellation & Returns</a></li>
                                     <li><a href="#">FAQ & Support Online</a></li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <div class="footer-bottom">
             <div class="container">
                 <div class="footer-bottom-wapper">
                    
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <span class="cw" style="color: white; font-size: 17px;">  Crafted with </span><i style="color:#960c0c; font-size: 17px;" class="fa fa-heart"></i> <span class="cw" style="color: white; font-size: 17px;">  By </span> <a href="http://shobarjonnoweb.com/"> <span style="color: white; font-size: 17px;">Shobarjonnoweb.com</span> </a>

           
                 </div>
             </div>
         </div>
     </div>
     <!-- ./footer paralax-->
</footer>