@extends('layouts.frontend')

@section('content')
<!-- HEADER -->

<!-- end header -->
{{-- @if (Session::has('message'))
        
<div class="alert alert-success" role="alert">
    <strong></strong><h4 style="text-align: center;"> {{Session::get('message')}}</h4>
</div>
      
@endif

 --}}

<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
       
        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            
<section id="gaming">
    <div class="grid-list-products">
        <h2 class="section-title">Search Results</h2>

        {{-- <div class="control-bar" style="height: 50px">
            <div class="grid-list-buttons">
                <ul>
                    <li class="grid-list-button-item active"><a data-toggle="tab" href="#grid-view"><i class="fa fa-th-large"></i> Grid View</a></li>
                    <li class="grid-list-button-item "><a data-toggle="tab" href="#list-view"><i class="fa fa-th-list"></i> List View</a></li>
                </ul>
            </div>

        </div><!-- /.control-bar --> --}}
           
        <div class="tab-content">
           {{--  <div id="grid-view" class="products-grid fade tab-pane in active">

                <div class="product-grid-holder">
                    <div class="row no-margin">
                          @include('frontend/ezbazzar/category_products_grid')
                    </div><!-- /.row -->
                </div><!-- /.product-grid-holder -->

            </div><!-- /.products-grid #grid-view --> --}}

            <div id="list-view" class="products-grid fade tab-pane in active">
                <div class="products-list">
                    
                    @include('frontend/ezbazzar/category_product_list')

                </div>

            </div><!-- /.products-grid #list-view -->

        </div><!-- /.tab-content -->
    </div><!-- /.grid-list-products -->

</section><!-- /#gaming -->
        </div>
        <!-- ./row-->
    </div>
</div>

@endsection