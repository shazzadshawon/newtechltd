<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>NewTech</title>
        
        @include('frontend.include.stylesheets')
    </head>

<body>
<style>
    .alert{
        margin-bottom: 0px;
        padding-left: 100px;
    }
</style>
    <div class="page-wrapper">

        <!-- Preloader -->
        <div class="preloader1"></div>

        <!-- Main Header / Header Style Two-->
        @include('frontend.include.header')
        <div class="" style="padding: 0;">
            <div class="row">
                <div class="col-md-12">
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('message') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('success') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('info'))
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('info') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('warning'))
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('warning') !!}</li>
                            </ul>
                        </div>
                    @endif
                    @if (Session::has('danger'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <ul>
                                <li>{!! Session::get('danger') !!}</li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <!--End Main Header -->


        @yield('content')
        <!--Main Slider-->
      

        <!--Main Footer-->
        @include('frontend.include.footer')
        <!--End Main Footer-->

    </div>
    <!--End pagewrapper-->

    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon fa fa-long-arrow-up"></span></div>

    @include('frontend.include.javascript')
</body>
</html>
