<script src="{{ asset('public/ezbazzar/js/bootstrap.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJc5UlxIHcSE5btuKK-gTiyDPIkjCjFmw" type="text/javascript"></script>
<script src="{{ asset('public/ezbazzar/js/gmap3.min.j') }}s"></script>
<script src="{{ asset('public/ezbazzar/js/bootstrap-hover-dropdown.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/css_browser_selector.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/echo.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/jquery.easing-1.3.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/bootstrap-slider.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/jquery.raty.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/jquery.prettyPhoto.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/jquery.customSelect.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/wow.min.js') }}"></script>
<script src="{{ asset('public/ezbazzar/js/scripts.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.min.js"></script>

<script src="{{ asset('public/ezbazzar/js/switchstylesheet.js') }}"></script>

<script>
    $(document).ready(function(){
        $(".changecolor").switchstylesheet( { seperator:"color"} );
        $('.show-theme-options').click(function(){
            $(this).parent().toggleClass('open');
            return false;
        });
    });

    $(window).bind("load", function() {
        $('.show-theme-options').delay(2000).trigger('click');
    });
</script>

<script src="http://w.sharethis.com/button/buttons.js"></script>