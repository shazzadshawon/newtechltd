<!DOCTYPE html>
<html lang="en">


{{--@include('layouts.frontend_css')--}}
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="Shobar Jonno Web">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/bootstrap.min.css') }}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Customizable CSS -->
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/green.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/animate.min.css') }}">
    <!--		<link href="public/ezbazzar/css/red.css" rel="alternate stylesheet" title="Red color">-->
    <link rel="stylesheet/less" type="text/css" href="{{ asset('public/ezbazzar/less/green.less') }}" />

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="{{ asset('public/ezbazzar/css/font-awesome.min.css') }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('public/ezbazzar/images/favicon_e.ico') }}">

    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
    <script src="public/ezbazzar/js/html5shiv.js"></script>
    <script src="public/ezbazzar/js/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('public/ezbazzar/js/jquery-1.10.2.min.js') }}"></script>
    <script src="{{ asset('public/ezbazzar/js/jquery-migrate-1.2.1.js') }}"></script>

</head>
<title>Home | Ezbazzar</title>
<body>

<div class="wrapper">
    <!-- ============================================================= TOP NAVIGATION ============================================================= -->

    @include('frontend.ezbazzar.header_top')
<!-- ============================================================= TOP NAVIGATION : END ============================================================= -->		<!-- ============================================================= HEADER ============================================================= -->
{{--@include('frontend.ezbazzar.header_top')--}}
@include('frontend.ezbazzar.header_main')
<div class="container">
     <div class="row">
                    <div class="col-md-12">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <ul>
                                        <li>{!! Session::get('success') !!}</li>
                                    </ul>
                                </div>
                            @endif
                        @if (Session::has('info'))
                            <div class="alert alert-info alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('info') !!}</li>
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('warning'))
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('warning') !!}</li>
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('danger'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <ul>
                                    <li>{!! Session::get('danger') !!}</li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
</div>
<!-- ============================================================= HEADER : END ============================================================= -->

@yield('content')
    <!-- ============================================================= FOOTER ============================================================= -->

@include('frontend.ezbazzar.footer')
<!-- ============================================================= FOOTER : END ============================================================= -->	</div><!-- /.wrapper -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
@include('layouts.frontend_js')

</body>
</html>