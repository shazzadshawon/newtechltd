<div class="row-blog">
    <div class="container">
        <!-- blog list -->
        <div class="blog-list">
            {{-- <h2 class="page-heading">
                <span class="page-heading-title">Available Brand</span>
            </h2> --}}
            <div class="blog-list-wapper">
                <ul class="owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":6}}'>

                            @php
                                 $offer = DB::table('pazzles')->where('publication_status',1)->Where('pazzle',4)->get();
                            @endphp
                            @foreach ($offer as $offerInfo)
                    <li>
                        <div class="post-thumb image-hover2">
                            <a href="#"><img src="../{{ $offerInfo->pazzle_image }}" alt="Blog" style = "height:80px; width:90px;"></a>
                        </div>
                      
                    </li>
                        
                        @endforeach
       
                   
                </ul>
            </div>
        </div>
        <!-- ./blog list -->
    </div>
</div>