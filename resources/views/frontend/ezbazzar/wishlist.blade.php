<!DOCTYPE html>
<html lang="en">
<?php require('head.php'); ?>
<title>Wishlist|EZBAZZAR</title>
<body>

<div class="wrapper">
    <!-- ============================================================= TOP NAVIGATION ============================================================= -->
    <?php require_once('header_top.php');?>
    <!-- ============================================================= TOP NAVIGATION : END ============================================================= -->		<!-- ============================================================= HEADER ============================================================= -->
    <?php require_once('header_main.php');?>
    <!-- ============================================================= HEADER : END ============================================================= -->



    <section id="cart-page">
        <div class="container">

            <div class="col-xs-12 col-sm-3 col-md-2 sidemenu-holder">
                <?php require('sidebar_navigation.php');?>
            </div><!-- /.sidemenu-holder -->

            <div class="col-xs-12 col-sm-9 col-md-10 homebanner-holder">
                <?php require('wishlist_content.php');?>
            </div><!-- /.homebanner-holder -->

        </div><!-- /.container -->
    </section>
    <!-- ============================================================= FOOTER ============================================================= -->
    <?php require('footer.php'); ?>
    <!-- ============================================================= FOOTER : END ============================================================= -->	</div><!-- /.wrapper -->


<!-- JavaScripts placed at the end of the document so the pages load faster -->
<?php require('javascripts.php'); ?>

</body>
</html>