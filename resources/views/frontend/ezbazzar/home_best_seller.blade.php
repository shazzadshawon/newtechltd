@php
$kount = array();
$kount[] = 0;
$kount2[] = 0;
// for ($x=0; $x < 1000000000; $x++) { 
//     $kount[$x] = 0;
// }
    $best_sells = DB::table('orders')->get();

    for ($i = 0; $i < count($best_sells); $i++){
        $kount[$best_sells[$i]->product_id] = DB::table('orders')->where('product_id',$best_sells[$i]->product_id)->count();

    }
    arsort($kount);
@endphp


<section id="bestsellers" class="color-bg wow fadeInUp">
    <div class="container">
        <h1 class="section-title">Best Sellers</h1>

        <div class="product-grid-holder medium">
            <div class="col-xs-12 col-md-7 no-margin product_multi_grit_con">

                <div class="row no-margin">
                @php
                    $a=1;
                @endphp

                @foreach($kount as $key => $value)

                    @php
                        $product = DB::table('products')->where('id',$key)->first();
                        $pro = (array) $product;
                        // if(!empty($pro))
                        // {
                        //     print_r($pro['product_name']);
                        // }
                    @endphp

                    @if (!empty($pro))
                    <div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
                        <div class="product-item">
                            <div class="image">
                            @php
                                $image =  DB::table('product_images')->where('product_id',$pro['id'])->first();
                                //print_r($image->product_image);
                            @endphp
                                <img style="height: 200px"  src="{{ asset('product_image/'.$image->product_image) }}" data-echo="{{ asset('product_image/'.$image->product_image) }}" />
                            </div>
                            <div class="body">
                                <div class="label-discount clear"></div>
                                <div class="title">
                                    <a href="{{ url('product-details/'.$pro['id']) }}">@php
                                       print_r($pro['product_name']);
                                    @endphp</a>
                                </div>
                                @php
                                    $subcat = DB::table('sub_categories')->where('sub_category_id',$pro['sub_category_id'])->first();
                                @endphp
                                <div class="brand">{{ $subcat->sub_category_name }}</div>
                            </div>
                            <div class="prices">

                                <div class="price-current text-right">&#2547;{{ $pro['product_price'] }}</div>
                            </div>
                            <div class="hover-area">
                                <div class="add-cart-button">
                                    <a href="{{ url('product-details/'.$pro['id']) }}" class="le-button">View Details</a>
                                </div>
                               {{--  <div class="wish-compare">
                                    <a class="btn-add-to-wishlist" href="#">Add to Wishlist</a>
                                </div> --}}
                            </div>
                        </div>
                    </div><!-- /.product-item-holder -->
                    @endif
                 @php
                    if ($a>5) {
                         break;
                     }
                     $a++;
                     
                 @endphp
                @endforeach
                  
  {{--               </div><!-- /.row -->

                <div class="row no-margin"> --}}


                </div><!-- /.row -->
            </div><!-- /.col -->


<!--            Single Product Image Slider-->
            <div class="col-xs-12 col-md-5 no-margin">
            @php
                //print_r($kount);
                reset($kount);
                $product_id = key($kount);
                $product = DB::table('products')->where('id',$product_id)->first();
                $subcat = DB::table('sub_categories')->where('sub_category_id',$product->sub_category_id)->first();
                $images =  DB::table('product_images')->where('product_id',$product_id)->get();
                $z=1;
            @endphp
                <div class="product-item-holder size-big single-product-gallery small-gallery product_image_slider">

                    <div id="best-seller-single-product-slider" class="single-product-slider owl-carousel">
                    @foreach ($images as $image)
                          <div class="single-product-gallery-item" id="slide{{ $z }}">
                            <a data-rel="prettyphoto" href="single-product.php">
                                <img alt="" src="{{ asset('product_image/'.$image->product_image) }}" data-echo="{{ asset('product_image/'.$image->product_image) }}" style="width: 433px; height: 450px;" />
                            </a>
                        </div><!-- /.single-product-gallery-item -->
                        @php
                            $z++;
                        @endphp
                    @endforeach
                      

                    </div><!-- /.single-product-slider -->

                    @php
                        $y=1;
                    @endphp

                    <div class="gallery-thumbs clearfix">
                    @foreach ($images as $image)
                        <ul>
                            <li><a class="horizontal-thumb @if ($y==1)  active
                            @endif" data-target="#best-seller-single-product-slider" data-slide="{{ $y-1 }}" href="#slide{{ $y }}"><img alt="" src="{{ asset('product_image/'.$image->product_image) }}" data-echo="{{ asset('product_image/'.$image->product_image) }}" style="width: 67px; height: 60px;" /></a></li>
                           
                        </ul>
                        @php
                            $y++;
                        @endphp
                    @endforeach
                    </div><!-- /.gallery-thumbs -->

                    <div class="body">
                        <div class="label-discount clear"></div>
                        <div class="title">
                            <a href="single-product.php">{{ $product->product_name }}</a>
                        </div>
                        <div class="brand">{{ $subcat->sub_category_name }}</div>
                    </div>
                    <div class="prices text-right">
                        <div class="price-current inline">&#2547;{{ $product->product_price }}</div>
                        <a href="{{ url('product-details/'.$product_id) }}" class="le-button big inline">View Details</a>
                    </div>
                </div><!-- /.product-item-holder -->
            </div><!-- /.col -->

        </div><!-- /.product-grid-holder -->
    </div><!-- /.container -->
</section><!-- /#bestsellers -->

<script type="text/javascript">
    var bs_height = $('#bestsellers .container .product_multi_grit_con').height();
    $('#bestsellers .product_image_slider').css('height', bs_height);
    console.log(bs_height);
</script>