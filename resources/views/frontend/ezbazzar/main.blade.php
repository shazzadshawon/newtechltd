@extends('layouts.frontend')

@section('content')


    <div id="top-banner-and-menu">
        <div class="container">

            <div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">

                @include('frontend.ezbazzar.sidebar_navigation')
            </div><!-- /.sidemenu-holder -->

            <div class="col-xs-12 col-sm-8 col-md-9 homebanner-holder">

                @include('frontend.ezbazzar.home_slider')
            </div><!-- /.homebanner-holder -->

        </div><!-- /.container -->
    </div><!-- /#top-banner-and-menu -->

    <!-- ========================================= CATEGORY SECTION ========================================= -->

    @include('frontend.ezbazzar.home_category')
    <!-- ========================================= CATEGORY SECTION END========================================= -->

    <!-- ========================================= BEST SELLERS ========================================= -->

    @include('frontend.ezbazzar.home_best_seller')
    <!-- ========================================= BEST SELLERS : END ========================================= -->
    <!-- ========================================= BEST SELLERS : END ========================================= -->
    <!-- ========================================= BEST SELLERS : END ========================================= -->

@endsection