@extends('layouts.frontend')

@section('content')


    <main id="about-us">
        <div class="container inner-top-xs inner-bottom-sm">

            <div class="row">
                <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">

                    <section id="who-we-are" class="section m-t-0">
                        <h2>Privacy policy</h2>
                        <p>When you use our Website, we collect and store your personal information which is provided by you from time to time. Our primary goal in doing so is to provide you a safe, efficient, smooth and customized experience. This allows us to provide services and features that most likely meet your needs, and to customize our website to make your experience safer and easier. More importantly, while doing so, we collect personal information from you that we consider necessary for achieving this purpose.</p>
                        <p>Below are some of the ways in which we collect and store your information:</p>
                        <ul class="list-group">
                            <li class="list-group-item">We receive and store any information you enter on our website or give us in any other way. We use the information that you provide for such purposes as responding to your requests, customizing future shopping for you, improving our stores, and communicating with you.</li>
                            <li class="list-group-item">We also store certain types of information whenever you interact with us. For example, like many websites, we use "cookies," and we obtain certain types of information when your web browser accesses ezbazarbd.comor advertisements and other content served by or on behalf of ezbazarbd.comon other websites.</li>
                            <li class="list-group-item">To help us make e-mails more useful and interesting, we often receive a confirmation when you open e-mail from ezbazarif your computer supports such capabilities.</li>
                        </ul>
                        <p>Information about our customers is an important part of our business, and we are not in the business of selling it to others. </p>
                        <p>We release account and other personal information when we believe release is appropriate to comply with the law; enforce or apply our Terms of Use and other agreements; or protect the rights, property, or safety of ezbazarbd.com, our users, or others. This includes exchanging information with other companies and organizations for fraud protection.</p>
                    </section><!-- /#who-we-are -->


                </div><!-- /.col -->
                <div class="col-xs-12 col-md-4 col-lg-4 col-sm-6">

                  
                    @include('frontend.ezbazzar.team')

                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </main><!-- /#about-us -->
  



  @endsection