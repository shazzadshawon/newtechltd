@extends('layouts.frontend')

@section('content')
    @include('frontend/ezbazzar/breadcrumb')
    <!-- ============================================================= HEADER : END ============================================================= -->		<section class="sidebar-page">
        <div class="container">
            <!-- ========================================= SIDEBAR ========================================= -->
            <div class="col-xs-12 col-sm-3 no-margin sidebar narrow sidemenu-holder">
                @include('frontend.ezbazzar.sidebar_navigation')
            </div>

        @php
            $subcat = DB::table('sub_categories')->where('sub_category_id',$productInfo->sub_category_id)->first();
        @endphp


            <!-- ========================================= SIDEBAR : END ========================================= -->

            <!-- ========================================= CONTENT ========================================= -->

            <div class="col-xs-12 col-sm-9 no-margin wide sidebar page-main-content">



            @include('frontend.ezbazzar.single_product-content')


                @include('frontend.ezbazzar.single_product-related_product')
                <!-- ========================================= RECENTLY VIEWED : END ========================================= -->
            </div>
        </div>

        @endsection

