@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->



    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>Contact Us</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!--Contact Form Section-->
    <section class="contact-form-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="column col-md-7 col-sm-12 col-sm-12">
                    <h2>SEND US MESSAGE</h2>
                    <!--                        <div class="text">Product management twitter rockstar mass market value proposition pivot venture partnership social proof hypotheses innovator founders.</div>-->

                    <!-- Contact Form -->
                    <div class="contact-form">

                        <!--Comment Form-->
                        <form method="post" action="{{url('post_contact')}}" id="contact-form">
                            {{csrf_field()}}
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="text" name="contact_title" placeholder="Name">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="email" name="contact_email" placeholder="Email">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="text" name="contact_phone" placeholder="Phone">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="text" name="contact_subject" placeholder="Subject">
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="contact_description" placeholder="message"></textarea>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <button class="theme-btn btn-style-two" type="submit" name="submit-form">Submit Now</button>
                                </div>

                            </div>
                        </form>

                    </div>
                    <!--End Contact Form -->

                </div>
                <div class="column col-md-5 col-sm-12 col-sm-12">
                    <h2>Location</h2>
                    <div class="map-outer">
                        <!--Map Canvas-->
                        <div class="map-canvas"
                             data-zoom="16"
                             data-lat="23.7788386"
                             data-lng="90.3933478"
                             data-type="roadmap"
                             data-hue="#ffc400"
                             data-title="NewTech Internation Ltd."
                             data-content="DOHS Mohakhali, Dhaka, Bangladesh<br><a href='mailto:contact@newtech.bd.com'>contact@newtech.bd.com</a>">
                        </div>
                    </div>

                    <!--Contact Info-->
                    <ul class="contact-info">
                        <li><div class="icon"><span class="flaticon-location-pin"></span></div>House# 457, Road# 31, DOHS Mohakhali , Dhaka-1206</li>
                        <li><div class="icon"><span class="flaticon-smartphone-1"></span></div>+880 1715113358</li>
                        <li><div class="icon"><span class="flaticon-e-mail-envelope"></span></div>contact@newtech.bd.com</li>
                        <li><div class="icon"><span class="flaticon-clock"></span></div>Mon - Sat: 10am - 5pm</li>
                    </ul>

                </div>
            </div>
        </div>
    </section>
    <!--End Contact Form Section-->


@endsection