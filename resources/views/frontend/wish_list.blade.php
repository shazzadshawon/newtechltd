@extends('layouts.frontend-newtech')

@section('content')
    <div class="sidebar-page-container shop-container">
        <div class="auto-container">
            <div class="cart-section">
                <!--Cart Outer-->
                <div class="cart-outer">
                    <div class="table-outer">
                        <table class="cart-table">
                            <thead class="cart-header">
                            <tr>
                                <th class="prod-column">No</th>
                                <th>&nbsp;</th>
                                <th>Item</th>
                                <th class="price">Price</th>
                                <th>Remove</th>
                            </tr>
                            </thead>

                            <tbody>

@php
$i=1;
@endphp

                            @foreach($wishlists as $wish)
                                <tr>
                                    @php
                                        $product = DB::table('products')->where('id',$wish->product_id)->first();
                                        $image = DB::table('product_images')->where('product_id',$wish->product_id)->first();
                                    @endphp
                                    <td>{{$i}}</td>
                                    <td colspan="2" class="prod-column">
                                        <div class="column-box">
                                            <figure class="prod-thumb"><img src="{{asset('public/uploads/product/'.$image->product_image)}}" alt=""></figure>
                                            <h4 class="prod-title">{{$product->product_name}}</h4>
                                        </div>
                                    </td>
                                    <td class="price">&#2547; {{$product->product_price}}</td>
                                    <td class="remove">
                                        <form action="{{URL::to('/remove-wishlist/'.$wish->id)}}">
                                            <button  type="submit" class="remove-btn" value=""><span class="flaticon-garbage"></span></button>

                                        </form>

                                    </td>
                                </tr>
                                @php
                                    $i++;
                                @endphp

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                    <div class="cart-options clearfix">

                        <div class="pull-right">
                            {{--<button type="button" class="theme-btn btn-style-six">Update List </button>--}}
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection