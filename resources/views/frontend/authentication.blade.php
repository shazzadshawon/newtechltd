@extends('layouts.frontend-newtech')
@section('content')
        <!-- ============================================================= HEADER : END ============================================================= -->
        <!-- ============================================================= HEADER : END ============================================================= -->		<!-- ========================================= MAIN ========================================= -->
        <main id="authentication" class="inner-bottom-md">
            <div class="container">
                <div class="row">

                    <div class="col-md-6">
                        <section class="section sign-in inner-right-xs">
                            <h2 class="bordered">Sign In</h2>
                            <p>Hello, Welcome to your account</p>

<!--                            <div class="social-auth-buttons">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6">-->
<!--                                        <button class="btn-block btn-lg btn btn-facebook"><i class="fa fa-facebook"></i> Sign In with Facebook</button>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <button class="btn-block btn-lg btn btn-twitter"><i class="fa fa-twitter"></i> Sign In with Twitter</button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->


                                {!! Form::open(['url' => '/customer-login-check', 'method'=>'POST', 'class' => 'login-form cf-style-1', 'role' => 'form']) !!}
                                <div class="form-group">
                                    <label class="">Email</label>
                                    <input type="email" class="form-control" name="email_address">
                                </div><!-- /.field-row -->

                                <div class="field-row">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="password">
                                </div><!-- /.field-row -->

                                <div class="field-row clearfix">
                                    <span class="pull-left">
                                        <label class="content-color"><input type="checkbox" class="le-checbox auto-width inline"> <span class="bold">Remember me</span></label>
                                    </span>
                                    {{--<span class="pull-right">--}}
                                        {{--<a href="#" class="content-color bold">Forgotten Password ?</a>--}}
                                    {{--</span>--}}
                                </div>

                                <div class="buttons-holder">
                                    <button type="submit" class="btn btn-warning">Sign In</button>
                                </div><!-- /.buttons-holder -->
                            {!! Form::close() !!}

                        </section><!-- /.sign-in -->
                    </div><!-- /.col -->

                    <div class="col-md-6">
                        <section class="section register inner-left-xs">
                            <h2 class="bordered">Create New Account</h2>
                            <p>Create your own Media Center account</p>


                            {!! Form::open(['route' => 'customer.store', 'class' => 'register-form cf-style-1', 'role' => 'form']) !!}
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email_address">
                                </div><!-- /.field-row -->

                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="customer_name">
                                </div><!-- /.field-row -->

                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" class="form-control" name="phone_number">
                                </div><!-- /.field-row -->

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" >
                                </div><!-- /.field-row -->

                                <div class="form-group">
                                    <label>Address</label>

                                    <textarea class="form-control" name="address"></textarea>
                                </div><!-- /.field-row -->



                                <div class="form-group">
                                    <button type="submit" class="btn btn-warning">Sign Up</button>
                                </div><!-- /.buttons-holder -->
                            {!! Form::close() !!}



                        </section><!-- /.register -->

                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.container -->
        </main><!-- /.authentication -->
    <!-- ========================================= MAIN : END ========================================= -->		<!-- ============================================================= FOOTER ============================================================= -->
   @endsection