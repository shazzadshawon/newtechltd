@extends('layouts.frontend-newtech')
@section('content')
        <!-- ============================================================= HEADER : END ============================================================= -->
        <!-- ============================================================= HEADER : END ============================================================= -->		<!-- ========================================= MAIN ========================================= -->
        <main id="authentication" class="inner-bottom-md">
            <div class="container">
                <div class="row">
                    <br><br>
                    <div class="col-md-6 col-md-offset-3">
                        <section class="section sign-in inner-right-xs">
                            <h2 class="bordered">Employee Login</h2>
                            <p>Hello, Welcome to your account</p>

<!--                            <div class="social-auth-buttons">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-6">-->
<!--                                        <button class="btn-block btn-lg btn btn-facebook"><i class="fa fa-facebook"></i> Sign In with Facebook</button>-->
<!--                                    </div>-->
<!--                                    <div class="col-md-6">-->
<!--                                        <button class="btn-block btn-lg btn btn-twitter"><i class="fa fa-twitter"></i> Sign In with Twitter</button>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->


                                {!! Form::open(['url' => '/employeelogincheck', 'method'=>'POST', 'class' => 'login-form cf-style-1', 'role' => 'form']) !!}
                                <div class="form-group">
                                    <label class="">Email</label>
                                    <input type="email" class="form-control" name="email_address">
                                </div><!-- /.field-row -->

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="password">
                                </div><!-- /.field-row -->

                                {{--<div class="field-row clearfix">--}}
                                    {{--<span class="pull-left">--}}
                                        {{--<label class="content-color"><input type="checkbox" class="le-checbox auto-width inline"> <span class="bold">Remember me</span></label>--}}
                                    {{--</span>--}}
                                  {{----}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Sign In</button>
                                </div><!-- /.buttons-holder -->
                            {!! Form::close() !!}

                        </section><!-- /.sign-in -->
                    </div><!-- /.col -->



                </div><!-- /.row -->
            </div><!-- /.container -->
        </main><!-- /.authentication -->
        <br><br><br>
    <!-- ========================================= MAIN : END ========================================= -->		<!-- ============================================================= FOOTER ============================================================= -->
   @endsection