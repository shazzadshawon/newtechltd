<section class="sponsors-section bg-violate">
    <div class="auto-container">
        <div class="carousel-outer">
            <!--Sponsors Slider-->
            <ul class="sponsors-carousel owl-carousel owl-theme">
                @foreach($brands as $brand)
                    <li><div class="image-box"><a href="" target="_blank"><img src="{{asset('public/uploads/brand/'.$brand->image)}}" alt="" style="width: 165px;"></a></div></li>
                @endforeach
            </ul>
        </div>
    </div>
</section>