<header class="main-header header-style-two">
    <!-- Header Top -->
    <div class="header-top">
        <div class="auto-container clearfix">
            <!--Top Left-->
            <div class="top-left pull-left">
                <ul class="links-nav clearfix">
                    <li>

                        Welcome to New Tech International Limited

                    </li>
                </ul>
            </div>

            <!--Top Right-->
            <div class="top-right pull-right">
                <ul class="links-nav clearfix">
                    <li><a href="#"><span class="icon flaticon-smartphone-1"></span> +880 1715113358</a></li>
                    <li><a href="#"><span class="icon flaticon-clock"></span> Mon - Sat: 10am - 5pm</a></li>
                </ul>
            </div>
        </div>
    </div><!-- Header Top End -->

    <!--Header-Upper-->
    <div class="header-upper">
        <div class="auto-container">
            <div class="clearfix">

                <div class="pull-left logo-outer">
                    <div class="logo"><a href="{{url('/')}}"><img src="{{asset('public/frontend/images/newtech_logo.png')}}" alt="newtech_logo" title="New Tech International Ltd"></a></div>
                </div>

                <div class="pull-right upper-right clearfix">

                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <li class="dropdown"><a href="#">About</a>
                                        <ul>
                                            <li><a href="{{url('about#office')}}">Office</a></li>
                                            <li><a href="{{url('about#showroom')}}">Showroom</a></li>
                                        </ul>
                                    </li>
                                    <?php 
                                    $main_category = DB::table('categories')->where('publication_status',1)->get();
                                     ?>
                                    <li class="dropdown"><a href="#">Shop</a>
                                        <ul>
                                        <?php foreach ($main_category as $main): ?>
                                            <?php
                                            $category = DB::table('sub_categories')->where('category_id',$main->category_id)->get();
                                            //$cat_count = DB::table('sub_categories')->where('category_id',$main->category_id)->count();
                                            ?>
                                             <li class="@if(count($category)>0)) dropdown @endif"><a href="#">{{$main->category_name}}</a>

                                                <ul>
                                                <?php foreach ($category as $cat): ?>
                                                     <li><a href="{{url('product_category/'.$cat->sub_category_id)}}">{{$cat->sub_category_name}}</a></li>
                                                <?php endforeach ?>
                                                </ul>
                                            </li>
                                        <?php endforeach ?>
                                           
                                         
                                        </ul>
                                    </li>
                                    <li><a href="#business">Our Business</a></li>
                                    <li class="dropdown"><a href="#">Service</a>
                                    <?php 
                                        $service_cats = DB::table('service_cat')->get();
                                    ?>
                                        <ul>
                                        <?php foreach ($service_cats as $cat): ?>
                                            <?php 
                                                $services = DB::table('services')->where('service_cat_id',$cat->id)->get();
                                                $count = DB::table('services')->where('service_cat_id',$cat->id)->count();
                                            ?>
                                            <li class="@if($count > 0) dropdown @endif"><a href="#" onclick="window.location.href= '{{url('service_cat/'.$cat->id)}}';">{{$cat->cat_name}}</a>
                                            
                                                <ul>
                                                <?php foreach ($services as $service): ?>
                                                     <li><a href="{{url('service/'.$service->id)}}">{{$service->service_title}}</a></li>
                                                <?php endforeach ?>
                                                </ul>
                                            </li>
                                        <?php endforeach ?>
                                            
                                           
                                        </ul>
                                    </li>
                                    <li><a href="{{url('achievements')}}">Achievement</a></li>
                                    <li><a href="#media_section">Media</a></li>
                                    <li><a href="{{url('contact')}}">Contact Us</a></li>
                                    <li class="dropdown"><a href="#"><i class="fa fa-user fa-2x"></i> </a>
                                        @if(\Session::has('customer_name') && \Session::has('customer_id'))
                                            <ul>


                                                <li class=""><a href="{{url('logout')}}">Logout  ( {{\Session::get('customer_name')}} )</a>

                                                </li>


                                            </ul>
                                        @else
                                            <ul>
                                                <li class=""><a href="{{url('join')}}">Login / Register</a>
                                            </ul>
                                        @endif
                                    </li>

                                    <li class="dropdown"><a style="" href="#"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                                        </a>

                                        <ul>

                                            <li class=""><a href="{{url('wishlist')}}">Wishlist</a>

                                            </li>
                                            <li class=""><a href="{{url('cart')}}">Shopping Cart</a>

                                            </li>
                                            <li class=""><a href="{{url('checkout')}}">Checkout</a>

                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->

                        <!--Quote Button-->
                        {{--<div class="btn-outer">--}}
                            {{--<!--Search Box-->--}}
                            {{--<div class="search-box-outer">--}}
                                {{--<div class="dropdown">--}}
                                    {{--<button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>--}}
                                    {{--<ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">--}}
                                        {{--<li class="panel-outer">--}}
                                            {{--<div class="form-container">--}}
                                                {{--<form method="post" action="index.php">--}}
                                                    {{--<div class="form-group">--}}
                                                        {{--<input type="search" name="field-name" value="" placeholder="Search Here" required>--}}
                                                        {{--<button type="submit" class="search-btn"><span class="fa fa-search"></span></button>--}}
                                                    {{--</div>--}}
                                                {{--</form>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>

                </div>

            </div>
        </div>
    </div>

    <!--Sticky Header-->
    <div class="sticky-header">
        <div class="auto-container clearfix">
            <!--Logo-->
            <div class="logo pull-left">
                <a href="{{url('/')}}" class="img-responsive"><img src="{{asset('public/frontend/images/newtech_logo.png')}}" alt="newtech_logo" title="New Tech International Ltd"></a>
            </div>

            <!--Right Col-->
            <div class="right-col pull-right">
                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li class="dropdown"><a href="#">About</a>
                                <ul>
                                    <li><a href="{{url('about')}}#office">Office</a></li>
                                    <li><a href="{{url('about')}}#showroom">Showroom</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="#">Shop</a>
                                <ul>
                                    <?php foreach ($main_category as $main): ?>
                                    <?php
                                    $category = DB::table('sub_categories')->where('category_id',$main->category_id)->get();
                                    //$cat_count = DB::table('sub_categories')->where('category_id',$main->category_id)->count();
                                    ?>
                                    <li class="@if(count($category)>0)) dropdown @endif"><a href="#">{{$main->category_name}}</a>

                                        <ul>
                                            <?php foreach ($category as $cat): ?>
                                            <li><a href="{{url('product_category/'.$cat->sub_category_id)}}">{{$cat->sub_category_name}}</a></li>
                                            <?php endforeach ?>
                                        </ul>
                                    </li>
                                    <?php endforeach ?>


                                </ul>
                            </li>
                            <li><a href="#business">Our Business</a></li>
                             <li class="dropdown"><a href="#">Service</a>
                                    <?php 
                                        $service_cats = DB::table('service_cat')->get();
                                    ?>
                                        <ul>
                                        <?php foreach ($service_cats as $cat): ?>
                                            <?php
                                            $services = DB::table('services')->where('service_cat_id',$cat->id)->get();
                                            $count = DB::table('services')->where('service_cat_id',$cat->id)->count();
                                            ?>
                                            <li class="@if($count > 0) dropdown @endif"><a href="{{url('service_cat/'.$cat->id)}}"  onclick="window.location.href= '{{url('service_cat/'.$cat->id)}}';">{{$cat->cat_name}}</a>

                                            <?php 
                                                $services = DB::table('services')->where('service_cat_id',$cat->id)->get();
                                            ?>
                                                <ul>
                                                <?php foreach ($services as $service): ?>
                                                     <li><a href="{{url('service/'.$service->id)}}">{{$service->service_title}}</a></li>
                                                <?php endforeach ?>
                                                </ul>
                                            </li>
                                        <?php endforeach ?>
                                            
                                           
                                        </ul>
                                    </li>
                            <li><a href="{{url('achievements')}}">Achievement</a></li>
                            <li><a href="#media_section">Media</a></li>
                            <li><a href="{{url('contact')}}">Contact Us</a></li>
                            <li class="dropdown"><a href="#"><i class="fa fa-user fa-2x"></i> </a>
                                @if(\Session::has('customer_name') && \Session::has('customer_id'))
                                    <ul>


                                        <li class=""><a href="{{url('logout')}}">Logout  ( {{\Session::get('customer_name')}} )</a>

                                        </li>


                                    </ul>
                                @else
                                    <ul>
                                        <li class=""><a href="{{url('join')}}">Login / Register</a>
                                    </ul>
                                @endif
                            </li>

                            <li class="dropdown"><a style="" href="#"><i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                                </a>

                                <ul>

                                    <li class=""><a href="{{url('wishlist')}}">Wishlist</a>

                                    </li>
                                    <li class=""><a href="{{url('cart')}}">Shopping Cart</a>

                                    </li>
                                    <li class=""><a href="{{url('checkout')}}">Checkout</a>

                                    </li>

                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav><!-- Main Menu End-->
            </div>

        </div>
    </div><!--End Sticky Header-->

</header>