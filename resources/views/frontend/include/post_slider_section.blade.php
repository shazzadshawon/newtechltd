<section class="services-section style-two">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Services Block-->
            <div class="services-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-wrench"></span>
                    </div>
                    <h3><a href="services-single.html">Creditable Integrity</a></h3>
                    <div class="text">Interaction design business-to-business low hanging fruit bootstrapping startup launch party user experience bandwidth web design.</div>
                    <a href="services-single.html" class="read-more">Read More</a>
                </div>
            </div>

            <!--Services Block-->
            <div class="services-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-helmet"></span>
                    </div>
                    <h3><a href="services-single.html">Effective Team Work</a></h3>
                    <div class="text">Interaction design business-to-business low hanging fruit bootstrapping startup launch party user experience bandwidth web design.</div>
                    <a href="services-single.html" class="read-more">Read More</a>
                </div>
            </div>

            <!--Services Block-->
            <div class="services-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon flaticon-award"></span>
                    </div>
                    <h3><a href="services-single.html">Quality Assurance</a></h3>
                    <div class="text">Interaction design business-to-business low hanging fruit bootstrapping startup launch party user experience bandwidth web design.</div>
                    <a href="services-single.html" class="read-more">Read More</a>
                </div>
            </div>

        </div>
    </div>
</section>