<section class="full-width-section bg-violate">
    <div class="outer-box clearfix">

        <!--Left Column-->
        <div class="left-column clearfix">
            <div class="content-box clearfix">
                <!--Sec Title Three-->
                <div class="sec-title-three">
                    <div class="title">Message of our MD</div>
                    <h2>Message</h2>
                </div>

                <div>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>

            </div>
        </div>

        <!--Right Column-->
        <div class="right-column" style="background-image:url(public/frontend/images/resource/full-width-1.jpg1)">
            <div class="counter-outer">

                <!--Fact Counter-->
                <div class="fact-counter style-two">
                    <div class="clearfix">

                        <img style="width: 100%; height: 300px;" src="public/frontend/images/resource/full-width-1.jpg">

                    </div>
                </div>

            </div>
        </div>

        <!--Side Img-->
        <!--  <div class="side-img"><img src="images/resource/side-img-1.png" alt=""/></div> -->
    </div>
</section>