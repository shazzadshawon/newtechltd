<section id="business" class="testimonial-section" style="background-image:url(public/frontend/images/background/3.jpg)">
    <div class="auto-container">
        <!--Sec Title Four-->
        <div class="sec-title-four">
            <div class="title">Clients</div>
            <h2>Our Businesses</h2>
        </div>
        <div class="three-item-carousel owl-carousel owl-theme">
        <?php foreach ($business as $bus): ?>
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="content">
                        <div class="image-box">
                            <img class="img-responsive" src="{{asset('public/uploads/business/'.$bus->image)}}" alt="" />
                        </div>
                        <h3>{{$bus->title}}</h3>
                        <div class="designation"><a href="{{$bus->description}}" target="_blank">Visit Website</a></div>
                    </div>
                </div>
            </div>

        <?php endforeach ?>
            <!--Testimonial Block-->
            
         

        </div>
    </div>
</section>