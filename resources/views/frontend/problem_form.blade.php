@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->



    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>Employee Area</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">Employee Area</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--Achievement Section 1-->
    <section id="office" class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <h4>Hello, {{\Session::get('employee_name')}}</h4>
                <a href="{{url('employee_logout')}}" class="btn btn-primary btn-lg" style="float: right;">Log out</a>
                <div class="col-md-12">
                    <form action="{{url('post_problem')}}" method="post">
                        {{csrf_field()}}
                    <section class="section sign-in inner-right-xs">

                        <h2 class="bordered">Employee Problem Form</h2>
                        <p>Please fill out all the fields to submit the issue</p>
                        <input type="hidden" name="employee_name" value="{{\Session::get('employee_name')}}">
                        <div class="form-group">
                            <label class="">Project Name</label>
                            <input type="text" class="form-control" name="project_name">
                        </div><!-- /.field-row -->

                        <div class="form-group">
                            <label class="">Employee Email Id</label>
                            <input type="text" class="form-control" name="email">
                        </div><!-- /.field-row -->

                        {{--<div class="form-group">--}}
                            {{--<label class="">Project Details</label>--}}
                            {{--<!--                                <input type="text" class="form-control" name="project_details">-->--}}
                            {{--<textarea class="form-control" name="project_details"></textarea>--}}
                        {{--</div><!-- /.field-row -->--}}

                        <div class="form-group">
                            <label class="">Problem Type</label>
                            <input type="text" class="form-control" name="problem_type">
                        </div><!-- /.field-row -->

                        <div class="form-group">
                            <label class="">Problem Details</label>
                            <textarea rows="5" class="form-control" name="problem_details"></textarea>
                        </div><!-- /.field-row -->

                        <div class="buttons-holder">
                            <button type="submit" class="btn btn-warning btn-block">Submit</button>
                        </div><!-- /.buttons-holder -->

                    </section><!-- /.sign-in -->
                    </form>
                </div><!-- /.col -->

            </div>
        </div>
    </section>
    <!--End Achievement Section 1-->


@endsection