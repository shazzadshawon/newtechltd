@extends('layouts.frontend-newtech')

@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(/newtech/public/frontend/images/background/5.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-12 col-xs-12">
                    <h1>Shopping Cart</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-12 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active">cart</li>
                        <li class="active"></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <div class="sidebar-page-container shop-container">
        <div class="auto-container">
            <div class="cart-section">
                <!--Cart Outer-->
                <div class="cart-outer">
                    <div class="table-outer">
                        <table class="cart-table">
                            <thead class="cart-header">
                            <tr>
                                <th class="prod-column">PRODUCT</th>
                                <th>&nbsp;</th>
                                <th>QUANTITY</th>
                                <th class="price">Price</th>
                                <th>Total</th>
                                <th>Remove</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            $total=0;
                            ?>
@foreach($cart_items as $item)
    @php
        $product = DB::table('products')->where('id',$item->product_id)->first();
        $image = DB::table('product_images')->where('product_id',$item->product_id)->first();

    @endphp
    <tr>
        <td colspan="2" class="prod-column">
            <div class="column-box">
                <figure class="prod-thumb"><img src="{{asset('public/uploads/product/'.$image->product_image)}}" alt=""></figure>
                <h4 class="prod-title">{{$product->product_name}}</h4>
            </div>
        </td>
        <td class="qty">{{$item->product_quantity}}</td>
        <td class="price">{{$item->product_price}}</td>
        <td class="sub-total">{{$item->product_price}}</td>
        <td class="remove"><a href="{{url('remove-cart-product/'.$item->id)}}" class="remove-btn"><span class="flaticon-garbage"></span></a></td>
    </tr>
    @php
    $total+=$item->product_price;
    @endphp
@endforeach


                            </tbody>
                        </table>
                    </div>



                    <div class="row clearfix">

                        <div class="column pull-right col-md-5 col-sm-8 col-xs-12">
                            <h3>Cart Totals</h3>

                            <!--Totals Table-->
                            <ul class="cart-totals-table">
                                {{--<li class="clearfix"><span class="col title">Sub Total</span><span class="col title">$215.00</span></li>--}}
                                <li class="clearfix total"><span class="col">Total</span><span class="col grand-total">${{$total}}</span></li>
                            </ul>

                            {{--<div class=""><button type="submit" class="theme-btn btn-style-two proceed-btn">Proceed to Checkout</button></div>--}}
                            <div class=""><a  class="theme-btn btn-style-two proceed-btn" href="{{url('checkout')}}">Checkout</a></div>
                </div>

                    </div>

                </div>
            </div>
        </div>
    </div>





@endsection