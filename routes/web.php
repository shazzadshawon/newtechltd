<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//
//Route::get('/', function () {
//    return view('pages.home');
//});
Route::group(['middleware'=>['web']],function(){
// Route::get('/admin','AdminController@index');
// Route::post('/dashboard','AdminController@AdminLoginCheck');

// Route::get('/dashboard','AdminMasterController@index');
// Route::get('/logout','AdminMasterController@logout');
Route::get('/search','AjaxController@search');


Route::resource('/subscribe','SubscribeController');
Route::get('/get-data','SubscribeController@gjsons');

Route::get('/get-data','SubscribeController@gjson');

Route::get('/location','HomeController@location');


//----------------------------------------------Start  View ----------------------------
Route::get('/', 'ViewContoller@index');

Route::resource('/customer','ViewContoller');
//Route::get('/product_search','ViewContoller@product_search');
Route::get('/product_category/{id}', 'ViewContoller@product_category');
//Route::get('/Main-Category-products/{id}', 'ViewContoller@ProductPageManin');
//Route::get('/Sub-Category-products/{id}', 'ViewContoller@ProductPageSub');
Route::get('/product-details/{id}', 'ViewContoller@SingleProductPage');
Route::get('/join', 'ViewContoller@CustomerLogin');
//Route::get('/Sign-Up', 'ViewContoller@CustomerSignUp');
Route::post('/customer-login-check', 'ViewContoller@CustomerLoginCheck');

Route::get('/employee_login', 'ViewContoller@employee_login');
    Route::get('/employee_login', function()
    {
        return view('frontend.employee_login');
    });
Route::post('/employeelogincheck', 'ViewContoller@employeelogincheck');
Route::get('/problem', 'ViewContoller@problem');
Route::post('/post_problem', 'ViewContoller@post_problem');
Route::post('/customer-login-check', 'ViewContoller@CustomerLoginCheck');

//Route::get('/Crazy-Deal/{id}', 'ViewContoller@CrazyDeal');
//Route::get('/Offer-Product/{id}', 'ViewContoller@OfferProduct');
Route::get('/logout', 'ViewContoller@logoutcustomer');
Route::get('/employee_logout', 'ViewContoller@employee_logout');
Route::get('/about', 'ViewContoller@about_us');

    Route::get('/service_cat/{id}','ViewContoller@service_cat');
    Route::get('/service/{id}','ViewContoller@service');
Route::get('/achievements', 'ViewContoller@achievements');

Route::get('/single_blog/{id}', function ($id){
    $blog = DB::table('blogs')->where('id',$id)->first();
    return view('frontend.single_blog',compact('blog'));
});

Route::get('/contact', 'ViewContoller@contact_us');
    Route::post('/post_contact', 'ViewContoller@post_contact');
//Route::get('/MD-Message', 'ViewContoller@md_message');
Route::get('/Tream-Condition', 'ViewContoller@treamCondition');
Route::get('/delivery_policy', function()
{
	return view('delivery_policy');
});
Route::get('/privacy', function()
{
	return view('frontend.ezbazzar.privecy');
});
Route::get('/terms', function()
{
	return view('frontend.ezbazzar.terms');
});
Route::get('/refund', function()
{
	return view('frontend.ezbazzar.refund');
});



//----------------------------------------------End  View ----------------------------

//----------------------------------------------Start  Wishlist ----------------------------

Route::resource('/wishlist','WishlistController');
Route::get('/view-wishlist','WishlistController@index');
Route::get('/remove-wishlist/{id}','WishlistController@destroy');
Route::get('/cart','ViewContoller@cart');
//----------------------------------------------End  Wishlist ----------------------------

//----------------------------------------------Start  AddToCart ----------------------------
Route::resource('/Add-To-Cart','AddToCartController');
Route::get('/My-Cart','AddToCartController@create');
Route::get('/remove-cart-product/{id}','AddToCartController@destroy');
Route::get('/checkout','AddToCartController@checkout');


//----------------------------------------------End  AddToCart ----------------------------

//----------------------------------------------Start  Order ----------------------------
Route::resource('/Order','OrderController');

});

Route::get('searchajax',array('as'=>'searchajax','uses'=>'AjaxController@autoComplete'));

Auth::routes();
$this->get('admin', 'Auth\LoginController@showLoginForm')->name('admin');
//$this->post('admin', 'Auth\LoginController@login');
$this->post('admin', 'Auth\LoginController@authenticate');



Route::get('/dashboard', 'HomeController@index');
Route::get('/shobarjonnoweb', 'HomeController@index');
//Route::get('/subscribers', 'HomeController@subscribers');
Route::get('/deletesubscriber/{id}', 'HomeController@deletesubscriber');
Route::get('/customers', 'HomeController@customers');
Route::get('/deletecustomer/{id}', 'HomeController@deletecustomer');

Route::group(['middleware'=>['auth']],function(){ 
    Route::get('/add-admin','HomeController@create');

    Route::get('/editvideo', 'VideoController@edit_video');
    Route::post('/updatevideo/', 'VideoController@update_video');

    Route::get('/edithotdeal', 'HotdealController@edit_hotdeal');
    Route::post('/updatehotdeal', 'HotdealController@update_hotdeal');


$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('add-admin');
$this->post('register', 'Auth\RegisterController@register');
//------------------------------------------------Start category ------------------------------
Route::get('/add-category','CategoryController@create');
Route::resource('/category', 'CategoryController');
Route::get('/manage-category', 'CategoryController@index');
Route::get('/delete-category/{id}', 'CategoryController@destroy');
Route::get('/unpublished-category/{id}', 'CategoryController@unpublished');
Route::get('/published-category/{id}', 'CategoryController@published');
//------------------------------------------------End category ------------------------------

//----------------------------------------------Start Sub Category ----------------------------
Route::get('/add-sub-category','SubCategoryController@create');
Route::resource('/sub-category', 'SubCategoryController');
Route::get('/manage-sub-category','SubCategoryController@index');
Route::get('/delete-sub-category/{id}', 'SubCategoryController@destroy');
Route::get('/unpublished-sub-category/{id}', 'SubCategoryController@unpublished');
Route::get('/published-sub-category/{id}', 'SubCategoryController@published');
//----------------------------------------------End Sub Category ----------------------------
//
////----------------------------------------------Start sub  Sub Category ----------------------------
//Route::get('/add-sub-sub-category','SubSubCategoryController@create');
//Route::resource('/sub-sub-category', 'SubSubCategoryController');
//Route::get('/manage-sub-sub-category','SubSubCategoryController@index');
//Route::get('/delete-sub-sub-category/{id}', 'SubSubCategoryController@destroy');
//Route::get('/unpublished-sub-sub-category/{id}', 'SubSubCategoryController@unpublished');
//Route::get('/published-sub-sub-category/{id}', 'SubSubCategoryController@published');
//----------------------------------------------End Sub Category ----------------------------

//----------------------------------------------Start Slider Image ----------------------------
Route::get('/add-slider-image','SliderImageController@create');
Route::resource('/slider-image','SliderImageController');
Route::get('/manage-slider-image','SliderImageController@index');
Route::get('/delete-slider-image/{id}', 'SliderImageController@destroy');
Route::get('/unpublished-slider-image/{id}', 'SliderImageController@unpublished');
Route::get('/published-slider-image/{id}', 'SliderImageController@published');
//----------------------------------------------End Slider Image ----------------------------

//----------------------------------------------Add Product ----------------------------
Route::get('/add-product','ProductController@create');
Route::resource('/product','ProductController');
Route::post('/select-sub-categories','AjaxController@SubCategory');
Route::get('/manage-product','ProductController@index');
Route::get('/product_callback','ProductController@product_callback');
Route::get('/delete-product/{id}', 'ProductController@destroy');
Route::get('/unpublished-product/{id}', 'ProductController@unpublished');
Route::get('/published-product/{id}', 'ProductController@published');
Route::get('/delete-product/{product_id}/image/{product_image_id}', 'ProductController@DeleteProductImage');
Route::get('/delete-product/{product_id}/size/{id}', 'ProductController@DeleteProductSize');
Route::post('/update_product_image/{product_id}/{product_image_id}', 'ProductController@UpdateProductImage');
//Route::post('/edit-product-image/{product_image_id}', 'ProductController@UpdateProductImage');
//Route::PUT('/update-product/{product_id}/image/{product_image_id}', 'ProductController@UpdateProductImage');
//Route::post('/add-product/{id}/image', 'ProductController@AddProductImage');
Route::post('/updateproductimage/{id}/image', 'ProductController@updateproductimage');
Route::post('/add-product/{id}/size', 'ProductController@AddProductSize');
//----------------------------------------------End  Product ----------------------------

Route::get('/manage-order','OrderController@index');
Route::get('/view-order/{id}','OrderController@ViewOrder');
Route::get('/delivered/{id}','OrderController@delivered');
Route::get('/refuse/{id}','OrderController@refuse');
Route::get('/delete-order/{id}','OrderController@destroy');




//-----------------------------------------------------------Offer-----------------------
//
//Route::get('/add-offer','PazzleController@create');
//
//Route::resource('/offer','PazzleController');
//Route::get('/manage-offer','PazzleController@index');
//Route::get('/setmegaoffer/{id}','PazzleController@setmegaoffer');
//Route::get('/delete-offer/{id}', 'PazzleController@destroy');
//Route::get('/unpublished-offer/{id}', 'PazzleController@unpublished');
//Route::get('/published-offer/{id}', 'PazzleController@published');
//
//Route::get('/subscribe','SubscribeController@store');
//Route::get('/show-subscribe','SubscribeController@index');
//Route::get('/show-message','SubscribeController@show_message');
//Route::get('/delete-subscribe/{id}','SubscribeController@destroy');
//
//





//  Blog section backend

    //  /blog
    Route::get('/allblogs', 'BlogController@index');
    Route::get('/addblog', 'BlogController@add');
    Route::post('/storeblog', 'BlogController@store');
    Route::get('/singleblog/{id}', 'BlogController@view');
    Route::get('/editblog/{id}', 'BlogController@edit');
    Route::post('/updateblog/{id}', 'BlogController@update');
    Route::get('/deleteblog/{id}', 'BlogController@destroy');

    //  /Achievement
    Route::get('/allachievements', 'AchievementController@index');
    Route::get('/addachievement', 'AchievementController@add');
    Route::post('/storeachievement', 'AchievementController@store');
    Route::get('/singleachievement/{id}', 'AchievementController@view');
    Route::get('/editachievement/{id}', 'AchievementController@edit');
    Route::post('/updateachievement/{id}', 'AchievementController@update');
    Route::get('/deleteachievement/{id}', 'AchievementController@destroy');


      //  /About
    Route::get('/allabouts', 'AboutController@index');
    Route::get('/addabout', 'AboutController@add');
    Route::post('/storeabout', 'AboutController@store');
    Route::get('/singleabout/{id}', 'AboutController@view');
    Route::get('/editabout/{id}', 'AboutController@edit');
    Route::post('/updateabout/{id}', 'AboutController@update');
    Route::get('/deleteabout/{id}', 'AboutController@destroy');


     //  /employee
    Route::get('/allemployees', 'UserController@index');
    Route::get('/addemployee', 'UserController@add');
    Route::post('/storeemployee', 'UserController@store');
    // Route::get('/singleemployee/{id}', 'UserController@view');
    Route::get('/editemployee/{id}', 'UserController@edit');
    Route::post('/updateemployee/{id}', 'UserController@update');
    Route::get('/deleteemployee/{id}', 'UserController@destroy');

    //  Brand
    Route::get('/allbrands', 'BrandController@index');
    Route::get('/addbrand', 'BrandController@add');
    Route::post('/storebrand', 'BrandController@store');
    Route::get('/singlebrand/{id}', 'BrandController@view');
    Route::get('/editbrand/{id}', 'BrandController@edit');
    Route::post('/updatebrand/{id}', 'BrandController@update');
    Route::get('/deletebrand/{id}', 'BrandController@destroy');

 //  Business
    Route::get('/allbusiness', 'BusinessController@index');
    Route::get('/addbusiness', 'BusinessController@add');
    Route::post('/storebusiness', 'BusinessController@store');
    Route::get('/singlebusiness/{id}', 'BusinessController@view');
    Route::get('/editbusiness/{id}', 'BusinessController@edit');
    Route::post('/updatebusiness/{id}', 'BusinessController@update');
    Route::get('/deletebusiness/{id}', 'BusinessController@destroy');



    // // Team    // 
    Route::get('/teammembers', 'TeamController@index');
    Route::get('/viewteammember/{id}', 'TeamController@show');
    Route::get('/addteammember', 'TeamController@add');
    Route::post('/storeteammember', 'TeamController@store');
    Route::get('/editteammember/{id}', 'TeamController@edit');
    Route::post('/updateteammember/{id}', 'TeamController@update');
    Route::get('/deleteteammember/{id}', 'TeamController@destroy');


    // // Gallery
    Route::get('/galleries', 'GalleryController@index');
    Route::get('/addgallery', 'GalleryController@add');
    Route::post('/storegallery', 'GalleryController@store');
    Route::post('/storegalleryvideo', 'GalleryController@storevideo');
    Route::get('/editgallery/{id}', 'GalleryController@edit');
    Route::post('/updategallery/{id}', 'GalleryController@update');
    Route::get('/deletegallery/{id}', 'GalleryController@destroy');

    //  /Service
    Route::get('/allservices', 'ServiceController@index');
    Route::get('/addservice', 'ServiceController@add');
    Route::post('/storeservice', 'ServiceController@store');
    Route::get('/singleservice/{id}', 'ServiceController@view');
    Route::get('/editservice/{id}', 'ServiceController@edit');
    Route::post('/updateservice/{id}', 'ServiceController@update');
    Route::get('/deleteservice/{id}', 'ServiceController@destroy');


    //  /Service Category
    Route::get('/allservicecategory', 'ServiceCategoryController@index');
    Route::get('/addservicecategory', 'ServiceCategoryController@add');
    Route::post('/storeservicecategory', 'ServiceCategoryController@store');
    Route::get('/singleservicecategory/{id}', 'ServiceCategoryController@view');
    Route::get('/editservicecategory/{id}', 'ServiceCategoryController@edit');
    Route::post('/updateservicecategory/{id}', 'ServiceCategoryController@update');
    Route::get('/deleteservicecategory/{id}', 'ServiceCategoryController@destroy');

    Route::get('/problems', 'SubscribeController@problems');
    Route::get('/deleteproblem/{id}', 'SubscribeController@deleteproblem');

});

// Route::get('admin','')