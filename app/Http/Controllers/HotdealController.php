<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class HotdealController extends Controller
{


    public function edit_hotdeal()
    {
        $hotdeal = DB::table('hotdeal')->first();
        //return $video->video_name;
        return view('backend.hotdeal.edithotdeal',compact('hotdeal'));
    }


    public function update_hotdeal()
    {

       
//return $fileName;
        DB::table('hotdeal')
            ->where('id',1)
            ->update([
                'discount' => Input::get('discount'),
                'description' => Input::get('editor1'),
                'end_date' => Input::get('end_date'),
                
            ]);

            if(Input::file('hotdealimage'))
            {

                $input = Input::all();
                $image = DB::table('hotdeal')->first();
                $name = $image->hotdealimage;

                //unlink('public/uploads/hotdeal/'.$name);

                $file = array_get($input,'hotdealimage');
                // SET UPLOAD PATH
                $destinationPath = 'public/uploads/hotdeal/';
                // GET THE FILE EXTENSION
                $extension = $file->getClientOriginalExtension();
                // RENAME THE UPLOAD WITH RANDOM NUMBER
                $fileName = time() . '.' . $extension;
                // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
                $upload_success = $file->move($destinationPath, $fileName);


                 DB::table('hotdeal')
                    ->where('id',1)
                    ->update([
                        
                        'hotdealimage' => $fileName,
                    ]);
            }




        return redirect()->back()->with('success','Hot Deal updated Successfully');

    }


}
