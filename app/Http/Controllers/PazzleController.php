<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use Redirect;
use Session;
use App\Category;
use App\SubCategory;
use App\Product;
use App\ProductImage;
use App\Pazzle;
use DB;

class PazzleController extends Controller
{


    public function index()
    {
        return view('backend.manage_offer');
    }



    public function create()
    {
        return view('backend.add_offer');
    }



    public function store(Request $request)
    {
         // getting all of the post data
      $file = $request->file('pazzle_image');
      // Making counting of uploaded images
      $file_count = count($file);
      // start count how many uploaded
      $uploadcount = 0;

     // foreach ($files as $file) {
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);
        if($validator->passes()){
          $destinationPath = 'pazzle_image/'; // upload folder in public directory
          $filename = $file->getClientOriginalName();
          $upload_success = $file->move($destinationPath, $filename);
          $uploadcount ++;

          // save into database
          $extension = $file->getClientOriginalExtension();
          $entry = new Pazzle();          
          $entry->pazzle_image = $filename;
          //$entry->heading = "null";
          $entry->pazzle = $request->pazzle;
          $entry->publication_status = $request->publication_status;
          $entry->save();
        }
      //}
      if($uploadcount == $file_count){
        Session::flash('success', 'Your Offer Images Has Been Uploaded successfully...!');
        return Redirect::to('/manage-offer');
      } else {
          Session::flash('success', 'Your Offer Images Has Been Uploaded successfully...!');
        return Redirect::to('/manage-offer');
      }
    }

    public function unpublished($id) {

//        $category = new Category;
        $slider_image = Pazzle::where('id', $id)
                ->update(['publication_status' => 0]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();


        Session::flash('success', 'Your Selected offer Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-offer');
    }
    
      public function published( $id)
    {
   
//        $category = new Category;
        $slider_image = Pazzle::where('id',$id)
                ->update(['publication_status' =>1]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();
      
        
            Session::flash('success', 'Your Selected offer Has Been published Successfully..!');
            return Redirect::to('/manage-offer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setmegaoffer($id)
    {
         DB::table('pazzles')
        ->update([
            'mega_offer'=> 0,
            ]);
        DB::table('pazzles')->where('id',$id)
        ->update([
            'mega_offer'=> 1,
            ]);
             Session::flash('success', 'Your Selected offer Has Been Set as Mega offer');
            return Redirect::to('/manage-offer');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pazzle = Pazzle::where('id',$id)->first();
        // return the view and pass in the var we previously created
        return view('backend.edit_offer')->withPazzle($pazzle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $data = array();
      
          $image = $request->file('pazzle_image');
//        $data['publication_status'] = $request->publication_status;
     
        if ($image!=NULL) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'pazzle_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
//                $data['slider_image'] = $image_url;
//                DB::table('slider_images')->insert($data);
                $slider_image = Pazzle::where('id',$id)
                ->update([
                    'pazzle_image' =>$image_full_name,
                    'pazzle' =>$request->pazzle,
                    'heading' =>$request->heading,
                    'publication_status' =>$request->publication_status
                    ]);
                Session::flash('success', 'Offer Updated Successfully...!');
                return Redirect::to('/manage-offer');
            }  else {
                Session::flash('success', 'Slider Image not seve..!');
                return Redirect::to('/manage-offer');
            }
        }else{
            $slider_image = Pazzle::where('id',$id)
                ->update(['pazzle' =>$request->pazzle,
                    'heading' =>$request->heading,
                    'publication_status' =>$request->publication_status]);
                Session::flash('success', 'Offer Updated Successfully...!');
                return Redirect::to('/manage-offer');
        }
    }


    
    public function destroy($id)
    {
        Pazzle::where('id', $id)->delete();
        Session::flash('success', 'Your Selected offer Image Has Been Deleted Successfully ....!');
        return Redirect::to('/manage-offer');
    }
}
