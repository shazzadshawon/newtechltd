<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class UserController extends Controller
{

   


    public function index()
    {
        $employees = DB::table('users')->orderby('id','desc')
                    ->get();
        return view('backend.employee.employees',compact('employees'));
    }

    


    public function add()
    {
        //$employeetypes = DB::table('employeetypes')->get();
        return view('backend.employee.addemployee');
    }

   



    public function store(Request $request)
    {

      if (Input::get('password') != Input::get('confirmpassword')) {
        return redirect('allemployees')->with('warning', 'Confirm password did not matched. Please try again !');
      }

      
      if(Input::file('image'))
      {
        $file = Input::file('image');
        $extension = $file->getClientOriginalExtension(); 
        $filename = time().'.'.$extension;



        Image::make(Input::file('image'))->save('public/uploads/employee/'.$filename);

        if(Input::get('role')==1)
        {
            DB::table('users')->insert(
                [
                    'name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'image' => $filename,
                    'address' => Input::get('address'),
                    'designation' => Input::get('designation'),
                    'password' => bcrypt(Input::get('password')),
                    'role' => Input::get('role'),
                    'status' => 1,
                ]
            );
        }
        else{
            DB::table('users')->insert(
                [
                    'name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'image' => $filename,
                    'address' => Input::get('address'),
                    'designation' => Input::get('designation'),
                    'password' => md5(Input::get('password')),
                    'role' => Input::get('role'),
                    'status' => 1,
                ]
            );
        }


      }

         return redirect('allemployees')->with('success', 'New Employee Added Successfully');
      
    }

   


 
    //  public function view( \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    // {
    //      $employees = DB::table('users')
    //                 ->where('employees.id',$id)
    //                 //->leftjoin('employeetypes','employees.employee_type_id','=','employeetypes.id')
    //                // ->select('employees.*','employeetypes.type_name')
    //                 ->get();
    //     //$employeetypes = DB::table('employeetypes')->get();
    //     $employee = $employees[0];
        
         
          
    //     session(['employee_id' => $employee->id]);
    //     session(['employee_title' => $employee->employee_title]);
         
    //     // if(!session_id()) {
    //     //     session_start();
    //     //     //$_SESSION["post_id"] = $cat->id;
    //     //     session(['wed' => '']);
    //     //     session(['wed' => $employee->id]);
    //     // }
        
        
        
        
      
    //      $login_link = $fb
    //         ->getRedirectLoginHelper()
    //         ->getLoginUrl('http://example.com/employee_callback', ['email','user_events']);
    //        // ->getLoginUrl('localhost/red_done_n/employee_callback', ['email', 'user_events']);
        
         
    //     return view('backend.employee.singleemployee',compact('employee','login_link'));
    // }

    
    
    
    



    public function edit($id)
    {
        $employees = DB::table('users')
                    ->where('id',$id)
                    ->get();
       
        $employee = $employees[0];
        //return $employees;
        return view('backend.employee.editemployee',compact('employee'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('users')
            ->where('id', $id)
            ->update([
                    'name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'address' => Input::get('address'),
                    'designation' => Input::get('designation'),
                    'status' => 1,
                    'role' => Input::get('role'),
                ]);
            if(Input::file('image'))
            {
                //return 'hy';
                 $file = Input::file('image');
                  $extension = $file->getClientOriginalExtension(); 
                  $filename = time().'.'.$extension;

                 Image::make(Input::file('image'))->save('public/uploads/employee/'.$filename);
                   DB::table('users')
                    ->where('id', $id)
                    ->update([
                            
                            'image' => $filename,
                            
                        ]);

            }
            if(Input::get('password'))
            {
                
                   DB::table('users')
                    ->where('id', $id)
                    ->update([
                            
                            'password' => bcrypt(Input::get('password')),
                            
                        ]);

            }

            return redirect('allemployees')->with('success', 'Employee Information Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ser = DB::table('users')->where('id', $id)->delete();
        //$image = $ser
        //if()


        return redirect('allemployees')->with('success', 'Selected Service removed Successfully');
    }



    
    
    
    
    public function employee_callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
           
        // if(!session_id()) {
        //     session_start();
            
        // }
         
   
         $post_id =  session('employee_id');
         $post_title =  session('employee_title');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //return typeof($accessToken);
          //$accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('131604694118980'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            //$accessToken = $oAuth2Client->getLongLivedAccessToken('EAABeioWfpIgBAHOc0XTZBwVKb4PZAbhzOfkiZAy3LoZCLPZAu9WlYkrjpEPC75RMhNZAnxLyyztpFRMZCpoyOZC7XgxXKNLMUEtw3ZCi3GqL06gVdQOmdtKL5wTHpDy3Xr7XIVt8Fq6ZBobmOMso49r8iHgJ6AsYVcc6gnu6ZBMO6Jea5eJLsU2rQvZCqs42UZBwmdZChhgSD3zeBZA9QZDZD');
            $accessToken = $oAuth2Client->getLongLivedAccessToken('131604694118980|oyTccHXJJnF-MGVpJ6Rls0TgWWs');
                                                                    
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
         // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://example.com/employee_callback/".$post_id,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/125126468121186/feed', $params);
                  return redirect('/shobarjonnoweb')->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        
        

    }

}
