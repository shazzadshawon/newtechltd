<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Session;
use App\Subscribe;
use DB;

class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.subscribes_user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gjson()
    {
        $subscribe = DB::table('subscribes')->first();
        return response()->json($subscribe);
    }

    public function gjsons()
    {
       
        return view('show')->with('info', json_decode(email, true));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,array(
           'email'=>'required|max:50'
       ));

        $subscribeInfo = DB::table('subscribes')->where('email',$request->email)->first();
        if($subscribeInfo!=NULL){
Session::flash('message','This Email is Already subscribed ..!');
        return Redirect::to('/');
        }else{
          $subscribe = new Subscribe;
      
       $subscribe->email = $request->email;
     
      
       $subscribe->save();
           Session::flash('message','You have been subscribed successfully..!');
        return Redirect::to('/');
       
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function problems()
    {
        $problems = DB::table('problems')->get();
        return view('backend.problems',compact('problems'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteproblem($id)
    {
        DB::table('problems')->where('id',$id)->delete();
        Session::flash('message', 'Message Removed Successfully');
        return \redirect()->back();
    }



    public function show_message()
    {
        $messages = DB::table('contacts')->get();
        //Session::flash('message', 'Message Successfully Sent');
        return view('backend.messages',compact('messages'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('subscribes')->where('id',$id)->delete();
        Session::flash('message', 'Your Selected user Has Been Deleted Successfully ....!');
            return Redirect::to('/show-subscribe');
    }
}
