<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\AddToCart;
use App\Category;
use App\Product;
use DB;

class AddToCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        $categories = Category::where('publication_status', 1)
                ->take(6)
                ->get();
        if (Session::has('customer_id')){
            $items = DB::table('add_to_carts')->where('session_id',Session::getId())->get();
            return view('frontend.checkout',compact('items'));
        }
        else
        {
            return  redirect()->back()->with('message','Please login before place an order');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        return view('cart');
    }


public function store(Request $request)
 {
     //return $request->all();
     //return $request->size;
    $total = 0;
     $product = Product::where('id', $request->product_id)->first();
    $addtocart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->get();
    foreach ($addtocart as $qt) {
        $total += $qt->product_quantity;
    }
    $total2 = $total + $request->product_quantity;

    if($total2 > $product->product_quantity)
    {
         Session::flash('message', 'Your Selected Product Already Stock Out..!');
            return Redirect()->back();
    }


        if($request->size!=NULL){
            //return $request->all();

            //$product   = Product::where('id', $request->product_id)->first();

            $addTocart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->where('size',$request->size)->first();
        if ($request->publication_status == 2) {
            Session::flash('message', 'Your Selected Product Already Stock Out..!');
//            return Redirect::to('/product-details/' . $request->product_id);
            return Redirect::to('cart');
        }

        elseif ($request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) 
        {

            if(!empty($addTocart)){

            $qty = $request->product_quantity + $addTocart->product_quantity;
            $cart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->where('size',$request->size)->update(['product_quantity' => $qty]);
            Session::flash('message', 'Your Selected Product Has Been Saved to Cart!');
            //return Redirect::to('/product-details/' . $request->product_id);
                return Redirect::to('cart');
            }
            else{

            $AddToCart = new AddToCart;
            $AddToCart->product_id = $request->product_id;
            $AddToCart->product_name = $request->product_name;
            $AddToCart->product_name_bn = $request->product_name_bn;
            $AddToCart->product_code = $request->product_code;
            $AddToCart->product_price = $request->product_price;
            $AddToCart->product_quantity = $request->product_quantity;
            $AddToCart->size = $request->size;
            $AddToCart->session_id = Session::getId();
                if ($AddToCart->save()) {
                    Session::flash('message', 'Your Selected Product Has Been Saved to Cart!');
                    //return Redirect::to('/product-details/' . $request->product_id);
                    return Redirect('cart');
                }
            }



        } 

       
        else {
            Session::flash('message', 'Sorry, Not Available Right now !');
            return Redirect::to('/product-details/' . $request->product_id);
        }
      //---------------------------Not use Size ------------------------
        }





        else{
           // return $request->all();

          // $product = Product::where('id', $request->product_id)->first();
        $addTocart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->first();
        if ($request->publication_status == 2) {
            Session::flash('message', 'Your Selected Product Already Stock Out..!');
            return Redirect::to('/product-details/' . $request->product_id);
        } elseif ($addTocart != NULL && $request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) {
            $qty = $request->product_quantity + $addTocart->product_quantity;
            $cart = AddToCart::where('product_id', $request->product_id)->where('session_id', Session::getId())->update(['product_quantity' => $qty]);
            Session::flash('message', 'Your Selected Product Has Been Saved to Cart!');
            return Redirect::to('/cart');
        } elseif ($request->product_quantity > 0 && $request->product_quantity <= $product->product_quantity) {
            $AddToCart = new AddToCart;
            $AddToCart->product_id = $request->product_id;
            $AddToCart->product_name = $request->product_name;
            $AddToCart->product_name_bn = $request->product_name_bn;
            $AddToCart->product_code = $request->product_code;
            $AddToCart->product_price = $request->product_price;
            $AddToCart->product_quantity = $request->product_quantity;
            $AddToCart->size = "None";
            $AddToCart->session_id = Session::getId();
            if ($AddToCart->save()) {
                Session::flash('message', 'Your Selected Product Has Been Saved to Cart!');
                return Redirect::to('/cart');
            }
        } else {
            Session::flash('message', 'Sorry, Not Abailable Right now !');
            return Redirect::to('/product-details/' . $request->product_id);
        }
        }
    }


      public function SaveOrder(Request $request){
          
      }
      
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AddToCart::where('id',$id)->where('session_id',Session::getId())->delete();
        Session::flash('message', 'Your Selected  Product Has Been Removed from the Cart ....!');
            return Redirect::back();
    }
}
