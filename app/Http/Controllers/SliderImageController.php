<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use Redirect;
use Session;
use App\SliderImage;
use DB;

class SliderImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $slider_image = SliderImage::all();
        return view('backend.manage_slider_image')->withSliderImage($slider_image);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.add_slider_image');
    }


    public function store(Request $request) {
      // getting all of the post data
      $file = $request->file('slider_image');
      // Making counting of uploaded images
      $file_count = count($file);
      // start count how many uploaded
      $uploadcount = 0;

      //foreach ($files as $file) {
        $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('file'=> $file), $rules);
        if($validator->passes()){
          $destinationPath = 'slider_image/'; // upload folder in public directory
          $filename = $file->getClientOriginalName();
          $upload_success = $file->move($destinationPath, $filename);
          $uploadcount ++;

          // save into database
          $extension = $file->getClientOriginalExtension();
          $entry = new SliderImage();          
          $entry->slider_image = $filename;
          $entry->title = $request->get('editor1');
          $entry->subtitle = $request->get('subtitle');
          $entry->publication_status = $request->publication_status;
          $entry->save();
        }
     // }
      if($uploadcount == $file_count){
        Session::flash('success', 'Your Slider Images Has Been Uploaded successfully...!');
        return Redirect::to('add-slider-image');
      } else {
          Session::flash('success', 'Your Slider Images Has Been Uploaded successfully...!');
        return Redirect::to('add-slider-image');
      }
    }
      public function unpublished($id) {

//        $category = new Category;
        $slider_image = SliderImage::where('slider_image_id', $id)
                ->update(['publication_status' => 0]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();


        Session::flash('success', 'Your Selected Has Been Unpublished Successfully..!');
        return Redirect::to('/manage-slider-image');
    }
    
      public function published( $id)
    {
   
//        $category = new Category;
        $slider_image = SliderImage::where('slider_image_id',$id)
                ->update(['publication_status' =>1]);
//        $category = Category::find($category_id);
//        $category->category_name = $request->category_name;
//        $category->publication_status = $request->publication_status;
//        $category->save();
      
        
            Session::flash('success', 'Your Selected Has Been published Successfully..!');
            return Redirect::to('/manage-slider-image');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $slider_image = SliderImage::where('slider_image_id',$id)->first();
        // return the view and pass in the var we previously created
        return view('backend.edit_slider_image')->withSliderImage($slider_image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $data = array();
      
          $image = $request->file('slider_image');
//        $data['publication_status'] = $request->publication_status;
     SliderImage::where('slider_image_id',$id)
                ->update([
                    'title' =>$request->get('editor1'),
                    'subtitle' =>$request->get('subtitle'),
                    'publication_status' =>$request->publication_status
                    ]);
        if ($image!=NULL) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'slider_image/';
            $image_url = $upload_path . $image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            if ($success) {
//                $data['slider_image'] = $image_url;
//                DB::table('slider_images')->insert($data);
                $slider_image = SliderImage::where('slider_image_id',$id)
                ->update([
                    'slider_image' =>$image_full_name
                   
                    ]);
                Session::flash('success', 'Slider Image Updated Successfully...!');
                return Redirect::to('/manage-slider-image');
            }  else {
                Session::flash('success', 'Slider Image not updated..!');
                return Redirect::to('/manage-slider-image');
            }
        }else{
            $slider_image = SliderImage::where('slider_image_id',$id)
                ->update(['publication_status' =>$request->publication_status]);
                Session::flash('success', 'Slider Image Updated Successfully...!');
                return Redirect::to('/manage-slider-image');
        }
    }


    public function destroy($id)
    {
        SliderImage::where('slider_image_id', $id)->delete();
        Session::flash('success', 'Your Selected Slider Image Has Been Deleted Successfully ....!');
        return Redirect::to('/manage-slider-image');
    }
}
