<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class BusinessController extends Controller
{

   


    public function index()
    {
        $business = DB::table('business')->orderby('id','desc')
                    ->get();
        return view('backend.business.business',compact('business'));
    }

    


    public function add()
    {
        //$businesstypes = DB::table('businesstypes')->get();
        return view('backend.business.addbusiness');
    }

   



    public function store(Request $request)
    {
        //$imgname = Input::get('name');
        $file = $request->file('name');
        $extension = $file->getClientOriginalExtension();
        $filename = time().'.'.$extension;



        Image::make(Input::file('name'))->save('public/uploads/business/'.$filename);

        DB::table('business')->insert(
        [
            'title' => Input::get('title'),
            'type_id' => 0,
            'image' => $filename,
            'description' => Input::get('link'),
            'status' => 1,
        ]
        );
        return redirect('allbusiness')->with('success', 'New business Added Successfully');
    }

   


 
     public function view( \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    {
         $business = DB::table('business')
                    ->where('business.id',$id)
                    //->leftjoin('businesstypes','business.type_id','=','businesstypes.id')
                   // ->select('business.*','businesstypes.type_name')
                    ->get();
        //$businesstypes = DB::table('businesstypes')->get();
        $business = $business[0];
        
         
          
        session(['id' => $business->id]);
        session(['title' => $business->title]);
         
        // if(!session_id()) {
        //     session_start();
        //     //$_SESSION["post_id"] = $cat->id;
        //     session(['wed' => '']);
        //     session(['wed' => $business->id]);
        // }
        
        
        
        
      
         $randin_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl('http://example.com/callback', ['email','user_events']);
           // ->getLoginUrl('localhost/red_done_n/callback', ['email', 'user_events']);
        
         
        return view('backend.business.singlebusiness',compact('business','randin_link'));
    }

    
    
    
    



    public function edit($id)
    {
        $business = DB::table('business')
                    ->where('id',$id)
                    ->get();
       
        $business = $business[0];
        //return $business;
        return view('backend.business.editbusiness',compact('business'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('business')
            ->where('id', $id)
            ->update([
                    'title' => Input::get('title'),
                    'description' => Input::get('link'),
                    'status' => 1,
                ]);
            if(Input::file('name'))
            {

              $business = DB::table('business')
                ->where('id', $id)->first();
              unlink('public/uploads/business/'.$business->image);
                

        $file = $request->file('name');
        $extension = $file->getClientOriginalExtension();
        $filename = time().'.'.$extension;

                 $filename = time().'.jpg';

                 Image::make(Input::file('name'))->save('public/uploads/business/'.$filename);
                   DB::table('business')
                    ->where('id', $id)
                    ->update([
                            
                            'image' => $filename,
                            
                        ]);

            }

            return redirect('allbusiness')->with('success', 'business Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $business = DB::table('business')->where('id', $id)->first();
        unlink('public/uploads/business/'.$business->image);
        DB::table('business')->where('id', $id)->delete();
        //$image = $ser
        //if()


        return redirect('allbusiness')->with('success', 'Selected business removed Successfully');
    }



    
    
    
    
    public function callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
           
        // if(!session_id()) {
        //     session_start();
            
        // }
         
   
         $post_id =  session('id');
         $post_title =  session('title');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //return typeof($accessToken);
          //$accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('131604694118980'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            //$accessToken = $oAuth2Client->getLongLivedAccessToken('EAABeioWfpIgBAHOc0XTZBwVKb4PZAbhzOfkiZAy3LoZCLPZAu9WlYkrjpEPC75RMhNZAnxLyyztpFRMZCpoyOZC7XgxXKNLMUEtw3ZCi3GqL06gVdQOmdtKL5wTHpDy3Xr7XIVt8Fq6ZBobmOMso49r8iHgJ6AsYVcc6gnu6ZBMO6Jea5eJLsU2rQvZCqs42UZBwmdZChhgSD3zeBZA9QZDZD');
            $accessToken = $oAuth2Client->getLongLivedAccessToken('131604694118980|oyTccHXJJnF-MGVpJ6Rls0TgWWs');
                                                                    
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is randged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
         // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-randin/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://example.com/callback/".$post_id,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/125126468121186/feed', $params);
                  return redirect('/shobarjonnoweb')->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        
        

    }

}
