<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class aboutController extends Controller
{

   


    public function index()
    {
        $abouts = DB::table('abouts')->orderby('id','desc')
                    ->get();
        return view('backend.about.abouts',compact('abouts'));
    }

    


    public function add()
    {
        //$abouttypes = DB::table('abouttypes')->get();
        return view('backend.about.addabout');
    }

   



    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';



        Image::make(Input::file('name'))->save('public/uploads/about/'.$filename);

        DB::table('abouts')->insert(
        [
            'title' => Input::get('title'),
            'type_id' => Input::get('type_id'),
            'image' => $filename,
            'description' => Input::get('editor1'),
            'status' => 1,
        ]
        );
         return redirect('allabouts')->with('success', 'New about Added Successfully');
    }

   


 
     public function view( \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    {
         $abouts = DB::table('abouts')
                    ->where('abouts.id',$id)
                    //->leftjoin('abouttypes','abouts.type_id','=','abouttypes.id')
                   // ->select('abouts.*','abouttypes.type_name')
                    ->get();
        //$abouttypes = DB::table('abouttypes')->get();
        $about = $abouts[0];
        
         
          
        session(['id' => $about->id]);
        session(['title' => $about->title]);
         
        // if(!session_id()) {
        //     session_start();
        //     //$_SESSION["post_id"] = $cat->id;
        //     session(['wed' => '']);
        //     session(['wed' => $about->id]);
        // }
        
        
        
        
      
         $login_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl('http://example.com/callback', ['email','user_events']);
           // ->getLoginUrl('localhost/red_done_n/callback', ['email', 'user_events']);
        
         
        return view('backend.about.singleabout',compact('about','login_link'));
    }

    
    
    
    



    public function edit($id)
    {
        $abouts = DB::table('abouts')
                    ->where('abouts.id',$id)
                    ->get();
       
        $about = $abouts[0];
        //return $abouts;
        return view('backend.about.editabout',compact('about'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('abouts')
            ->where('id', $id)
            ->update([
                    'title' => Input::get('title'),
                    'description' => Input::get('editor1'),
                    'type_id' => Input::get('type_id'),
                    'status' => 1,
                ]);
            if(Input::file('name'))
            {
                //return 'hy';
                 $filename = time().'.jpg';
                  $ser = DB::table('abouts')->where('id', $id)->first();
                  //var_dump($ser);exit;
                    unlink('public/uploads/about/'.$ser->image);

                 Image::make(Input::file('name'))->save('public/uploads/about/'.$filename);
                   DB::table('abouts')
                    ->where('id', $id)
                    ->update([
                            
                            'image' => $filename,
                            
                        ]);

            }

            return redirect('allabouts')->with('success', 'about Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ser = DB::table('abouts')->where('id', $id)->first();
        unlink('public/uploads/about/'.$ser->image);
        DB::table('abouts')->where('id', $id)->delete();
     

        return redirect('allabouts')->with('success', 'Selected about removed Successfully');
    }



    
    
    
    
    public function callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
           
        // if(!session_id()) {
        //     session_start();
            
        // }
         
   
         $post_id =  session('id');
         $post_title =  session('title');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //return typeof($accessToken);
          //$accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('131604694118980'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            //$accessToken = $oAuth2Client->getLongLivedAccessToken('EAABeioWfpIgBAHOc0XTZBwVKb4PZAbhzOfkiZAy3LoZCLPZAu9WlYkrjpEPC75RMhNZAnxLyyztpFRMZCpoyOZC7XgxXKNLMUEtw3ZCi3GqL06gVdQOmdtKL5wTHpDy3Xr7XIVt8Fq6ZBobmOMso49r8iHgJ6AsYVcc6gnu6ZBMO6Jea5eJLsU2rQvZCqs42UZBwmdZChhgSD3zeBZA9QZDZD');
            $accessToken = $oAuth2Client->getLongLivedAccessToken('131604694118980|oyTccHXJJnF-MGVpJ6Rls0TgWWs');
                                                                    
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
         // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://example.com/callback/".$post_id,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/125126468121186/feed', $params);
                  return redirect('/shobarjonnoweb')->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        
        

    }

}
